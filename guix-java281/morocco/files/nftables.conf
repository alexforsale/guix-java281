table inet global
delete table inet global

define localnet = { 10.0.10.0/24, 172.16.0.0/12, 10.254.254.0/24, 169.254.0.0/16, 192.168.0.0/16 }
define localnet_v6 = { fd00::/8, fe80::/10, 2001:470:ec4a::/48 }
define dev_lan = intern0
define dev_world = extern0
define dev_tun = sit1

table inet global {
  chain inbound_world {
    meta l4proto . th dport vmap {tcp . 80 : accept, tcp . 443 : accept, tcp . 2222 : accept, tcp . 9993 : accept, tcp . 29994 : accept}
  }

  chain inbound_local {
    meta l4proto . th dport vmap {
      tcp . 22 : accept, udp . 53 : accept, tcp . 53 : accept, udp . 67 : accept, 67 . udp : accept,
      tcp . 5353 : accept, udp . 5353 : accept, tcp . 631 : accept, udp . 631 : accept, tcp . 547 : accept,
      udp . 547 : accept, tcp . 853 : accept, udp . 853 : accept, tcp . 9993 : accept, tcp . 29994 : accept,
      udp . 3478 : accept, udp . 4379 : accept, udp . 4380 : accept
    }
    meta l4proto { tcp, udp } th dport 27015-27050 counter accept comment "steam"
    ip saddr $localnet counter accept
    ip6 saddr $localnet_v6 counter accept
  }

  chain inbound {
    type filter hook input priority 0; policy drop;

    ct state vmap { established : accept, related : accept, invalid : drop }
    iifname lo counter accept
    meta l4proto ipv6-icmp icmpv6 type { nd-neighbor-advert, nd-neighbor-solicit, nd-router-advert} ip6 hoplimit 1 counter accept
    meta l4proto ipv6-icmp icmpv6 type { nd-neighbor-advert, nd-neighbor-solicit, nd-router-advert} ip6 hoplimit 255 counter accept
    meta protocol ip icmp type { echo-request } counter accept
    ip protocol igmp counter accept
    iifname vmap { $dev_world : jump inbound_world, $dev_lan : jump inbound_local, $dev_tun : jump inbound_world }
    ip saddr $localnet counter jump inbound_local
    ip6 saddr $localnet_v6 jump inbound_local
    counter reject with icmpx port-unreachable
  }

  chain forward {
    type filter hook forward priority 0; policy drop;

    ct state vmap { established : accept, related : accept, invalid : drop }
    meta l4proto ipv6-icmp icmpv6 type { nd-neighbor-advert, nd-neighbor-solicit, nd-router-advert} ip6 hoplimit 1 counter accept
    meta l4proto ipv6-icmp icmpv6 type { nd-neighbor-advert, nd-neighbor-solicit, nd-router-advert} ip6 hoplimit 255 counter accept
    meta protocol ip icmp type { echo-request } counter accept
    jump inbound_local
  }

  chain postrouting {
    type nat hook postrouting priority 100; policy accept;

    ip saddr $localnet meta oifname $dev_world counter masquerade
    ip saddr 10.254.254.0/24 ip daddr $localnet counter masquerade
    ip saddr 10.0.10.0/24 ip daddr $localnet counter masquerade
    ip saddr 172.16.0.0/12 ip daddr $localnet counter masquerade
  }

  chain prerouting {
    type nat hook prerouting priority dstnat; policy accept;
  }
}
