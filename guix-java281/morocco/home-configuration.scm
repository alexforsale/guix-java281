;; This "home-environment" file can be passed to 'guix home reconfigure'
;; to reproduce the content of your profile.  This is "symbolic": it only
;; specifies package names.  To reproduce the exact same profile, you also
;; need to capture the channels being used, as returned by "guix describe".
;; See the "Replicating Guix" section in the manual.
(define-module (guix-java281 morocco home-configuration)
  #:use-module (gnu home)
  #:use-module (gnu home services)
  #:use-module (gnu home services mcron)
  #:use-module (gnu home services shepherd)
  #:use-module (gnu packages)
  #:use-module (gnu packages mail)
  #:use-module (gnu services)
  #:use-module (guix gexp)
  #:use-module (guix-java281 packages sync)
  #:use-module (guix-java281 home services)
  #:use-module (guix-java281 home modules emacs)
  #:use-module (guix-java281 home modules fonts)
  #:use-module (guix-java281 home modules desktop)
  #:use-module (guix-java281 home modules mail)
  #:use-module (guix-java281 home modules misc)
  #:use-module (guix-java281 home modules shells))

(define %grive-timer-job
  ;; every 15 minutes (I hope).
  #~(job
     '(next-minute-from
       (next-minute (range 0 60 30)))
     (string-append #$grive2 "/bin/grive "
                    "-V "
                    "-p " (string-append (getenv "HOME") "/Documents/google-drive"))))

(define %offlineimap-gmail-timer-job
  #~(job
     '(next-minute-from
       (next-minute (range 0 60 30)))
     (string-append #$offlineimap3 "/bin/offlineimap "
                    "-s " "-q "
                    "-a " "gmail")))

(define %offlineimap-yahoo-timer-job
  #~(job
     '(next-minute-from
       (next-minute (range 0 60 45)))
     (string-append #$offlineimap3 "/bin/offlineimap "
                    "-s " "-q "
                    "-a " "yahoo")))

(define %offlineimap-ymail-timer-job
  #~(job
     '(next-hour-from
       (next-hour (range 0 24 2)))
     (string-append #$offlineimap3 "/bin/offlineimap "
                    "-s " "-q "
                    "-a " "yahoo")))

(define %offlineimap-hotmail-timer-job
  #~(job
     '(next-hour-from
       (next-hour (range 0 24 4)))
     (string-append #$offlineimap3 "/bin/offlineimap "
                    "-s " "-q "
                    "-a " "hotmail")))

(home-environment
 (packages
  `(,@emacs-packages
    ,@desktop-packages
    ,@fonts-packages
    ,@bash-packages
    ,@lang-packages
    ,@misc-packages
    ,@mail-packages
    ))
 (services
  `(,@modular-profile-services
    ,@modular-profile-extensions
    ,@global-variables
    ,@font-services
    ,@bash-services
    ,@emacs-configuration
    ,@desktop-services
    ,@screen
    ,@go-path
    ,@nix-path
    ,@node-path
    ,@guix-environment
    ,@cargo-path
    ,@ruby-path
    ,@password-store-configuration
    ,@xsettingsd-configuration
    ,@gtk-configuration
    ,@rofi-configuration
    ,@xorg-user-configuration
    ,(service home-mcron-service-type
              (home-mcron-configuration
               (jobs (list
                      %grive-timer-job
                      %offlineimap-gmail-timer-job
                      %offlineimap-yahoo-timer-job
                      %offlineimap-ymail-timer-job
                      %offlineimap-hotmail-timer-job))))
    ,(simple-service
      'wallpaper
      home-files-service-type
      `((".local/share/wallpapers/morocco.png"
         ,(local-file "../home/files/morocco.png"))))
    ,@email-services
    ,(service
      home-shepherd-service-type
      (home-shepherd-configuration
       (services
        (list
         xsettingsd-shepherd-service
         ;;emacs-shepherd-service
         ;;offlineimap-shepherd-service
         ))))
    )))
