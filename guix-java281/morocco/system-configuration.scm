(define-module (guix-java281 morocco system-configuration)
  #:use-module (guix-java281 system services)
  #:use-module (guix-java281 system services network)
  #:use-module (guix gexp)
  #:use-module (gnu system)
  #:use-module (gnu system accounts)
  #:use-module (gnu system file-systems)
  #:use-module (gnu system keyboard)
  #:use-module (gnu system shadow)
  #:use-module (gnu system nss)
  #:use-module (gnu services)
  #:use-module (gnu services admin)
  #:use-module (gnu services desktop)
  #:use-module (gnu services dns)
  #:use-module (gnu services docker)
  #:use-module (gnu services mcron)
  #:use-module (gnu services networking)
  #:use-module (gnu services sddm)
  #:use-module (gnu services spice)
  #:use-module (gnu services sysctl)
  #:use-module (gnu services virtualization)
  #:use-module (gnu services xorg)
  #:use-module (gnu packages)
  #:use-module (gnu packages admin)
  #:use-module (gnu packages dns)
  #:use-module (gnu packages docker)
  #:use-module (gnu packages networking)
  #:use-module (gnu packages lua)
  #:use-module (gnu packages shells)
  #:use-module (gnu packages xorg)
  #:use-module (nongnu packages linux)
  #:use-module (nongnu system linux-initrd)
  #:use-module (srfi srfi-1))

(define %morocco-packages
  (list
   docker docker-cli docker-compose
   netcat-openbsd))

(define radvd-config
  (local-file "files/radvd.conf"))

(define dhcp4-config
  (local-file "files/dhcp4.conf"))

(define dhcp6-config
  (local-file "files/dhcp6.conf"))

(define %java281-morocco-services
  (append
   (list
    (service unattended-upgrade-service-type)
    (service docker-service-type
             (docker-configuration
              (enable-iptables? #f)))
    (service qemu-binfmt-service-type
             (qemu-binfmt-configuration
              (platforms
               (lookup-qemu-platforms
                "arm" "aarch64"))))
    (service libvirt-service-type)
    (service virtlog-service-type)
    (service
     nftables-service-type
     (nftables-configuration
      (ruleset (local-file "files/nftables.conf")))))
   (modify-services
       %java281-common-desktop-services
     (sysctl-service-type config =>
                          (sysctl-configuration
                           (settings (append '(;; from https://www.knot-dns.cz/docs/3.1/singlehtml/index.html#simple-configuration
                                               ("net.core.wmem_max" . "1048576")
                                               ("net.core.wmem_default" . "1048576")
                                               ("net.core.rmem_max" . "1048576")
                                               ("net.core.rmem_default" . "1048576")
                                               ("net.core.busy_read" . "0")
                                               ("net.core.busy_poll" . "0")
                                               ("net.core.netdev_max_backlog" . "40000")
                                               ("net.core.optmem_max" . "20480")
                                               ("vm.swappiness" . "30")
                                               ("net.ipv4.conf.all.forwarding" . "1")
                                               ("net.ipv6.conf.default.forwarding" . "1")
                                               ("net.ipv6.conf.all.forwarding" . "1"))
                                             %default-sysctl-settings))))
     (network-manager-service-type config =>
                                   (network-manager-configuration
                                    (dns "none")))
     (elogind-service-type
      config => (elogind-configuration (inherit config)
                                       (handle-power-key 'ignore))))))

(define-zone-entries homelab.java281.dynv6.net.zone
  ("@" "" "IN" "A" "10.0.10.254")
  ("@" "" "IN" "AAAA" "2001:470:ec4a:3fff::ffff")
  ("@" "" "IN" "ns" "morocco")
  ("morocco" "" "IN" "A" "10.0.10.254")
  ("morocco" "" "IN" "A" "10.254.254.254")
  ("morocco" "" "IN" "AAAA" "2001:470:ec4a:3fff::ffff"))

(define-zone-entries java281.dynv6.net.zone
  ("@" "" "IN" "A" "10.0.10.254")
  ("@" "" "IN" "AAAA" "2001:470:ec4a:3fff::ffff")
  ("@" "" "IN" "ns" "morocco")
  ("morocco" "" "IN" "A" "10.0.10.254")
  ("morocco" "" "IN" "A" "10.254.254.254")
  ("morocco" "" "IN" "AAAA" "2001:470:ec4a:3fff::ffff"))

(define homelab-zone
  (knot-zone-configuration
   (domain "homelab.java281.dynv6.net")
   (zone (zone-file
          (origin "homelab.java281.dynv6.net")
          (entries homelab.java281.dynv6.net.zone)))
   (acl '("homelab"))))

(define java281-zone
  (knot-zone-configuration
   (domain "java281.dynv6.net")
   (zone (zone-file
          (origin "java281.dynv6.net")
          (entries java281.dynv6.net.zone)))
   (acl '("homelab"))))

(define homelab-acl
  (knot-acl-configuration
   (id "homelab")
   (address '("10.0.10.0/24"))
   (action '("update"))))

(operating-system
  (inherit %java281-base-operating-system)
  (host-name "morocco")
  (keyboard-layout (keyboard-layout "us" "altgr-intl"))
  (kernel linux)
  (kernel-arguments
   (append
    '("consoleblank=300")
    %default-kernel-arguments))
  (initrd microcode-initrd)
  (firmware (list linux-firmware))
  (file-systems
   (cons*
    (file-system
      (device (file-system-label "EFI"))
      (mount-point "/boot/efi")
      (type "vfat"))
    (file-system
      (device
       (file-system-label "root"))
      (mount-point "/")
      (type "btrfs")
      (options "subvol=guix,compress=zstd"))
    (file-system
      (device
       (file-system-label "btrfs0"))
      (mount-point "/home")
      (type "btrfs")
      (options "subvol=guix-home,compress=zstd"))
    (file-system
      (device
       (file-system-label "btrfs0"))
      (mount-point "/data/Music")
      (type "btrfs")
      (options "subvol=Music,compress=zstd"))
    (file-system
      (device
       (file-system-label "btrfs0"))
      (mount-point "/data/Series")
      (type "btrfs")
      (options "subvol=Series,compress=zstd"))
    (file-system
      (device
       (file-system-label "btrfs0"))
      (mount-point "/data/Movies")
      (type "btrfs")
      (options "subvol=Movies,compress=zstd"))
    (file-system
      (device
       (file-system-label "btrfs0"))
      (mount-point "/data/Manga")
      (type "btrfs")
      (options "subvol=Manga,compress=zstd"))
    (file-system
      (device
       (file-system-label "btrfs0"))
      (mount-point "/data/Docker")
      (type "btrfs")
      (options "subvol=Docker,compress=zstd"))
    (file-system
      (device
       (file-system-label "btrfs0"))
      (mount-point "/data/Downloads")
      (type "btrfs")
      (options "subvol=Downloads,compress=zstd"))
    (file-system
      (device
       (file-system-label "btrfs0"))
      (mount-point "/data/vm")
      (type "btrfs")
      (options "subvol=vm,compress=zstd"))
    (file-system
      (device
       (file-system-label "btrfs0"))
      (mount-point "/data/ISO")
      (type "btrfs")
      (options "subvol=ISO,compress=zstd"))
    %base-file-systems))
  (swap-devices
   (list
    (swap-space
     (target (file-system-label "swap")))))
  (groups %java281-local-groups)
  (users
   (cons*
    (user-account
     (name "alexforsale")
     (comment "Kristian Alexander P")
     (uid 1000)
     (home-directory "/home/alexforsale")
     (group "alexforsale")
     (supplementary-groups '("wheel" "netdev" "users" "realtime"
                             "input" "lp" "audio" "video" "libvirt"
                             "docker" "kvm" "network-manager")))
    %java281-local-users))
  (packages
   (append
    (list knot `(,knot "lib") `(,knot "tools") knot-resolver
          lua5.2-ossl lua5.2-socket
          socat)
    %morocco-packages
    %java281-desktop-packages
    %i3-packages
    %exwm-packages
    %stumpwm-packages
    %bash-packages
    %java281-basic-packages))
  (services
   (append
    (list
     (service
      sddm-service-type
      (sddm-configuration
       ;;(theme "maya")
       (hide-users "admin-local")
       (xorg-configuration
        (xorg-configuration
         (keyboard-layout keyboard-layout)
         (extra-config (list %xorg-libinput-config))))))
     (service
      dhcpd-service-type
      (dhcpd-configuration
       (version "4")
       (config-file dhcp4-config)
       (interfaces '("intern0"))))
     ;; (service
     ;;  dhcpd-service-type
     ;;  (dhcpd-configuration
     ;;   (version "6")
     ;;   (config-file dhcp6-config)
     ;;   (interfaces '("intern0"))))
     ;; (service
     ;;  dnsmasq-service-type
     ;;  (dnsmasq-configuration
     ;;   (no-resolv? #t)
     ;;   (servers '("1.1.1.1" "9.9.9.9" "10.0.10.254#53000"))
     ;;   (local-service? #f)
     ;;   ;;(listen-addresses '("10.0.10.254"))
     ;;   (addresses
     ;;    '("/homelab.java281.dynv6.net/10.0.10.254"
     ;;      "/java281.dynv6.net/10.0.10.254"))
     ;;   (cache-size 1000)))
     (service knot-service-type
              (knot-configuration
               (includes '("/etc/knot/ddns.key"))
               (zones (list homelab-zone
                            java281-zone))
               (listen-port 53000)
               (acls `(,homelab-acl))))
     (service knot-resolver-service-type
              (knot-resolver-configuration
               (kresd-config-file
                (plain-file
                 "kresd.conf"
                 "\
     net.listen('10.0.10.254', 53, { kind = 'dns' })
     net.listen('2001:470:ec4a:3fff::ffff', 53, { kind = 'dns'})
     net.listen('10.0.10.254', 853, { kind = 'tls' })
     net.listen('2001:470:ec4a:3fff::ffff', 853, { kind = 'tls' })
     net.tls(\"/data/Docker/homelab/certs/java281.dynv6.net/java281.dynv6.net.pem\", \"/data/Docker/homelab/certs/java281.dynv6.net/key.pem\")
     modules = {
      'view',
      'hints > iterate',  -- Allow loading /etc/hosts or custom root hints
      'stats',            -- Track internal statistics
      'predict',          -- Prefetch expiring/frequent records
     }
     cache.size = 100 * MB
     log_level('info')
     internalDomains = policy.todnames({'java281.dynv6.net', 'homelab.java281.dynv6.net'})
     view:addr('127.0.0.0/8', policy.all(policy.PASS))
     view:addr('::1', policy.all(policy.PASS))
     view:addr('192.168.0.0/16', policy.all(policy.PASS))
     view:addr('10.0.10.0/24', policy.all(policy.PASS))
     view:addr('172.16.0.0/12', policy.all(policy.PASS))
     view:addr('2001:470:ec4a::/48', policy.all(policy.PASS))
     policy.add(policy.suffix(policy.FORWARD({'10.0.10.254@53000', '2001:470:ec4a:3fff::ffff@53000'}), internalDomains))
     policy.TLS_FORWARD({
       {'9.9.9.9', hostname='dns.quad9.net'},
       {'2620:fe::fe', hostname='dns.quad9.net'},
       {'1.1.1.1', hostname='cloudflare-dns.com'},
       {'1.0.0.1', hostname='cloudflare-dns.com'},
       {'2606:4700:4700::1111', hostname='cloudflare-dns.com'},
       {'2606:4700:4700::1001', hostname='cloudflare-dns.com'}
     })
     -- policy.add(policy.all(policy.FORWARD(
     --   {'1.1.1.1', '1.0.0.1',
     --    '9.9.9.9', '2620:fe::fe',
     --    '2606:4700:4700::1111', '2606:4700:4700::1001'})))
     -- "))))
     (service
      radvd-service-type
      (radvd-configuration
       (config-file radvd-config)))
     (service
      ddclient-service-type))
    %java281-common-nongnu-services
    %java281-common-services
    %java281-morocco-services))
  (name-service-switch %mdns-host-lookup-nss))
