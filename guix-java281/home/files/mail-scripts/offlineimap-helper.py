#!/usr/bin/env python
"""Offlineimap helper script"""

from subprocess import check_output

# this is for python3.10
# def get_pass(account):
#     """Return password for ACCOUNT."""
#     match account:
#        case "yahoo":
#            return check_output(
#                "pass login.yahoo.com/app_pass/alexforsale@yahoo.com",
#                shell=True).decode('utf-8').strip('\n')
#        case "ymail":
#            return check_output(
#                "pass login.yahoo.com/app_pass/christian.alexander@ymail.com",
#                shell=True).decode('utf-8').strip('\n')
#        case "gmail":
#             return check_output(
#                 "pass google.com/app_pass/alexarians@gmail.com",
#                 shell=True).decode('utf-8').strip('\n')
#        case "hotmail":
#             return check_output(
#                 "pass outlook.live.com/christian.alexander@windowslive.com",
#                 shell=True).decode('utf-8').strip('\n')

yahoo_args = "pass login.yahoo.com/app_pass/alexforsale@yahoo.com"
ymail_args = "pass login.yahoo.com/app_pass/christian.alexander@ymail.com"
gmail_args = "pass google.com/app_pass/alexarians@gmail.com"
hotmail_args = "pass outlook.live.com/christian.alexander@windowslive.com"

def get_pass(account):
    if account == "yahoo":
        return check_output(
            yahoo_args, shell=True).decode('utf-8').strip('\n')
    elif account == "ymail":
        return check_output(
            ymail_args, shell=True).decode('utf-8').strip('\n')
    elif account == "gmail":
        return check_output(
            gmail_args, shell=True).decode('utf-8').strip('\n')
    elif account == "hotmail":
        return check_output(
            hotmail_args, shell=True).decode('utf-8').strip('\n')
