#!/bin/sh
# ~/.config/xprofile.d/00-security.sh
# various ui-related security settings
# <alexforsale@yahoo.com>
if [ "$(command -v gnome-keyring-daemon)" ] &&
       [ ! "$(pgrep -u ${USER} -x gnome-keyring-d)" ];then
   eval "$(gnome-keyring-daemon --start --components=pkcs11,secrets,ssh)" >/dev/null 2>&1
   export SSH_AUTH_SOCK GNOME_KEYRING_CONTROL
fi

[ "$(command -v dbus-update-activation-environment)" ] &&
    dbus-update-activation-environment DISPLAY
