#!/usr/bin/env bash
export HISTFILE="$BASH_DATA"/bash_history
if [ -f "$HOME/.bash_history " ]; then
    if [ -f "$HISTFILE" ]; then
        cat "$HOME"/.bash_history >> "$HISTFILE"
    fi
    rm "$HOME"/.bash_history
fi
HISTSIZE=10000
HISTFILESIZE=2000000
shopt -s histappend
HISTCONTROL=ignoreboth
HISTIGNORE='ls:ll:ls -alh:pwd:clear:history'
HISTTIMEFORMAT='%F %T '
shopt -s cmdhist
