#!/usr/bin/env bash
#!/usr/bin/env bash
# Set colorful PS1 only on colorful terminals.
# dircolors --print-database uses its own built-in database
# instead of using /etc/DIR_COLORS.  Try to use the external file
# first to take advantage of user additions.
# We run dircolors directly due to its changes in file syntax and
# terminal name patching.

for sh in /etc/bash/bashrc.d/* ; do
    [[ -r ${sh} ]] && source "${sh}"
done

use_color=false
if type -P dircolors >/dev/null ; then
    # Enable colors for ls, etc.  Prefer ~/.dir_colors #64489
    LS_COLORS=
    if [[ -f ~/.dir_colors ]] ; then
        eval "$(dircolors -b ~/.dir_colors)"
    elif [[ -f /etc/DIR_COLORS ]] ; then
        eval "$(dircolors -b /etc/DIR_COLORS)"
    else
        eval "$(dircolors -b)"
    fi
    # Note: We always evaluate the LS_COLORS setting even when it's the
    # default.  If it isn't set, then `ls` will only colorize by default
    # based on file attributes and ignore extensions (even the compiled
    # in defaults of dircolors). #583814
    if [[ -n ${LS_COLORS:+set} ]] ; then
        use_color=true
    else
        # Delete it if it's empty as it's useless in that case.
        unset LS_COLORS
    fi
else
    # Some systems (e.g. BSD & embedded) don't typically come with
    # dircolors so we need to hardcode some terminals in here.
    case ${TERM} in
        [aEkx]term*|rxvt*|gnome*|konsole*|screen*|cons25|*color|linux) use_color=true;;
    esac
fi

# bash prompt variables
#
# \a
#     A bell character.
# \d
#     The date, in "Weekday Month Date" format (e.g., "Tue May 26").
# \D{format}
#     The format is passed to strftime(3) and the result is inserted
#     into the prompt string; an empty format results in a locale-specific
#     time representation. The braces are required.
# \e
#     An escape character.
# \h
#     The hostname, up to the first ‘.’.
# \H
#     The hostname.
# \j
#     The number of jobs currently managed by the shell.
# \l
#     The basename of the shell’s terminal device name.
# \n
#     A newline.
# \r
#     A carriage return.
# \s
#     The name of the shell, the basename of $0 (the portion
#     following the final slash).
# \t
#     The time, in 24-hour HH:MM:SS format.
# \T
#     The time, in 12-hour HH:MM:SS format.
# \@
#     The time, in 12-hour am/pm format.
# \A
#     The time, in 24-hour HH:MM format.
# \u
#     The username of the current user.
# \v
#     The version of Bash (e.g., 2.00)
# \V
#     The release of Bash, version + patchlevel (e.g., 2.00.0)
# \w
#     The current working directory, with $HOME abbreviated with a
#     tilde (uses the $PROMPT_DIRTRIM variable).
# \W
#     The basename of $PWD, with $HOME abbreviated with a tilde.
# \!
#     The history number of this command.
# \#
#     The command number of this command.
# \$
#     If the effective uid is 0, #, otherwise $.
# \nnn
#     The character whose ASCII code is the octal value nnn.
# \\
#     A backslash.
# \[
#     Begin a sequence of non-printing characters. This could be used to
#     embed a terminal control sequence into the prompt.
# \]
#     End a sequence of non-printing characters.

# ANSI color codes
# RS="\[\033[0m\]"    # reset
# HC="\[\033[1m\]"    # hicolor
# UL="\[\033[4m\]"    # underline
# INV="\[\033[7m\]"   # inverse background and foreground
# FBLK="\[\033[30m\]" # foreground black
# FRED="\[\033[31m\]" # foreground red
# FGRN="\[\033[32m\]" # foreground green
# FYEL="\[\033[33m\]" # foreground yellow
# FBLE="\[\033[34m\]" # foreground blue
# FMAG="\[\033[35m\]" # foreground magenta
# FCYN="\[\033[36m\]" # foreground cyan
# FWHT="\[\033[37m\]" # foreground white
# BBLK="\[\033[40m\]" # background black
# BRED="\[\033[41m\]" # background red
# BGRN="\[\033[42m\]" # background green
# BYEL="\[\033[43m\]" # background yellow
# BBLE="\[\033[44m\]" # background blue
# BMAG="\[\033[45m\]" # background magenta
# BCYN="\[\033[46m\]" # background cyan
# BWHT="\[\033[47m\]" # background white
# txtblk='\e[0;30m' # Black - Regular
# txtred='\e[0;31m' # Red
# txtgrn='\e[0;32m' # Green
# txtylw='\e[0;33m' # Yellow
# txtblu='\e[0;34m' # Blue
# txtpur='\e[0;35m' # Purple
# txtcyn='\e[0;36m' # Cyan
# txtwht='\e[0;37m' # White
# bldblk='\e[1;30m' # Black - Bold
# bldred='\e[1;31m' # Red
# bldgrn='\e[1;32m' # Green
# bldylw='\e[1;33m' # Yellow
# bldblu='\e[1;34m' # Blue
# bldpur='\e[1;35m' # Purple
# bldcyn='\e[1;36m' # Cyan
# bldwht='\e[1;37m' # White
# unkblk='\e[4;30m' # Black - Underline
# undred='\e[4;31m' # Red
# undgrn='\e[4;32m' # Green
# undylw='\e[4;33m' # Yellow
# undblu='\e[4;34m' # Blue
# undpur='\e[4;35m' # Purple
# undcyn='\e[4;36m' # Cyan
# undwht='\e[4;37m' # White
# bakblk='\e[40m'   # Black - Background
# bakred='\e[41m'   # Red
# bakgrn='\e[42m'   # Green
# bakylw='\e[43m'   # Yellow
# bakblu='\e[44m'   # Blue
# bakpur='\e[45m'   # Purple
# bakcyn='\e[46m'   # Cyan
# bakwht='\e[47m'   # White
# txtrst='\e[0m'    # Text Resetreset='\033[0m'

if ${use_color} ; then
    if [[ ${EUID} == 0 ]] ; then
        PS1='\[\033[01;31m\]\h\[\033[01;34m\] \w \$\[\033[00m\] '
    else
        #PS1='\[\033[01;32m\]\u@\h\[\033[01;34m\] \w \$\[\033[00m\] '
        case "${DISTRO}" in
            # nothing fancy, just different colors for hostname
            # blue for arch derivatives
            "artix"|"arch")
                PS1="\[\033[0;32m\]\u\[\033[00m\]\e[2m\[\033[1m\]@\[\033[00m\]\[\033[00m\]\[\033[0;34m\]\h\[\033[00m\]\[\033[01;34m\] \w \$\[\033[00m\] "
                [[ -n "${GUIX_ENVIRONMENT}" ]] && PS1="\[\033[0;32m\]\u\[\033[00m\]\e[2m\[\033[1m\]@\[\033[00m\]\[\033[00m\]\[\033[0;34m\]\h\[\033[00m\]\[\033[01;34m\] \w [env]\$\[\033[00m\] "
                ;;
            "debian"|"devuan"|"ubuntu")
                # purple for debian and such
                PS1="\[\033[0;32m\]\u\[\033[00m\]\e[2m\[\033[1m\]@\[\033[00m\]\[\033[00m\]\[\033[0;35m\]\h\[\033[00m\]\[\033[01;34m\] \w \$\[\033[00m\] "
                [[ -n "${GUIX_ENVIRONMENT}" ]] && PS1="\[\033[0;32m\]\u\[\033[00m\]\e[2m\[\033[1m\]@\[\033[00m\]\[\033[00m\]\[\033[0;35m\]\h\[\033[00m\]\[\033[01;34m\] \w [env]]\$\[\033[00m\] "
                ;;
            "freebsd")
                # red
                PS1="\[\033[0;32m\]\u\[\033[00m\]\e[2m\[\033[1m\]@\[\033[00m\]\[\033[00m\]\[\033[1;31m\]\h\[\033[00m\]\[\033[01;34m\] \w \$\[\033[00m\] "
                ;;
            "gentoo")
                # bold blue
                PS1="\[\033[0;32m\]\u\[\033[00m\]\e[2m\[\033[1m\]@\[\033[00m\]\[\033[00m\]\[\033[1;36\]\h\[\033[00m\]\[\033[01;34m\] \w \$\[\033[00m\] "
                [[ -n "${GUIX_ENVIRONMENT}" ]] && PS1="\[\033[0;32m\]\u\[\033[00m\]\e[2m\[\033[1m\]@\[\033[00m\]\[\033[00m\]\[\033[1;36\]\h\[\033[00m\]\[\033[01;34m\] \w [env]\$\[\033[00m\] "
                ;;
            "guix")
                #Yellow
                PS1="\[\033[0;32m\]\u\[\033[00m\]\e[2m\[\033[1m\]@\[\033[00m\]\[\033[00m\]\[\033[1;33m\]\h\[\033[00m\]\[\033[01;34m\] \w \$\[\033[00m\] "
                [[ -n "${GUIX_ENVIRONMENT}" ]] && PS1="\[\033[0;32m\]\u\[\033[00m\]\e[2m\[\033[1m\]@\[\033[00m\]\[\033[00m\]\[\033[1;33m\]\h\[\033[00m\]\[\033[01;34m\] \w [env]\$\[\033[00m\] "
                ;;
        esac
   fi
else
    # show root@ when we don't have colors
    PS1+='\u@\h \w \$ '
fi

                   # Try to keep environment pollution down, EPA loves us.
                   unset use_color sh
