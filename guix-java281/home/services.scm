(define-module (guix-java281 home services)
  #:use-module (guix gexp)
  #:use-module (gnu home services)
  #:use-module (gnu home services shells)
  #:use-module (gnu services))

(define-public screen
  (list
   (simple-service
    'screenrc
    home-files-service-type
    `((".config/screen/config"
       ,(local-file "files/screenrc"))))
   (simple-service
    'screen-env
    home-environment-variables-service-type
    '(("SCREENDIR" . "${XDG_CONFIG_HOME}/screen")
      ("SCREENRC" . "${SCREENDIR}/config")))))

(define-public go-path
  (list
   (simple-service
    'go-env
    home-environment-variables-service-type
    '(("GOPATH" . "${HOME}/.local")))))

(define-public nix-path
  (list
   (simple-service
    'nixpath
    home-environment-variables-service-type
    '(("PATH" . "${HOME}/.nix-profile/bin:${PATH}")
      ("XCURSOR_PATH" . "${HOME}/.nix-profile/share/icons:${XCURSOR_PATH}")))
   (simple-service
    'nix-profile
    home-shell-profile-service-type
    (list
     (plain-file
      "nix-profile"
      "\
[ -e /run/current-system/profile/etc/profile.d/nix.sh ] && . /run/current-system/profile/etc/profile.d/nix.sh
[ -e /run/current-system/profile/etc/profile.d/nix-daemon.sh ] && . /run/current-system/profile/etc/profile.d/nix-daemon.sh
")))))

(define-public node-path
  (list
   (simple-service
    'nodepath
    home-environment-variables-service-type
    '(("npm_config_prefix" . "${HOME}/.local")))))

(define-public guix-environment
  (list
   (simple-service
    'guix-build-options
    home-environment-variables-service-type
    '(("GUIX_BUILD_OPTIONS" . "${GUIX_BUILD_OPTIONS} -v 3 -c 2 -M 2")))))

(define-public cargo-path
  (list
   (simple-service
    'cargo-path
    home-environment-variables-service-type
    '(("PATH" . "${HOME}/.cargo/bin:${PATH}")))))

(define-public ruby-path
  (list
   (simple-service
    'ruby-path
    home-environment-variables-service-type
    '(("PATH" . "$(ruby -e 'print Gem.user_dir')/bin:${PATH}")))))

(define-public password-store-configuration
  (list
   (simple-service
    'pass-link
    home-shell-profile-service-type
    (list
     (plain-file
      "pass-link"
      "\
if [ -f /run/current-system/profile/lib/password-store/extensions/otp.bash ]; then
    [ ! -L \"${HOME}/.password-store/.extensions/otp.bash\" ] &&
        ln -s /run/current-system/profile/lib/password-store/extensions/otp.bash \
           \"${HOME}/.password-store/.extensions/\"
fi
")))
   (simple-service
    'pass-extensions
    home-environment-variables-service-type
    '(("PASSWORD_STORE_ENABLE_EXTENSIONS" . "true")))))
