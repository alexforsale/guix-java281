(define-module (guix-java281 home modules misc)
  #:use-module (guix-java281 packages sync)
  #:use-module (gnu packages)
  #:use-module (gnu packages admin)
  #:use-module (gnu packages aspell)
  #:use-module (gnu packages check)
  #:use-module (gnu packages cmake)
  #:use-module (gnu packages commencement)
  #:use-module (gnu packages crates-io)
  #:use-module (gnu packages dns)
  #:use-module (gnu packages games)
  #:use-module (gnu packages gawk)
  #:use-module (gnu packages golang)
  #:use-module (gnu packages guile)
  #:use-module (gnu packages guile-xyz)
  #:use-module (gnu packages haskell)
  #:use-module (gnu packages haskell-apps)
  #:use-module (gnu packages llvm)
  #:use-module (gnu packages lisp-xyz)
  #:use-module (gnu packages lua)
  #:use-module (gnu packages networking)
  #:use-module (gnu packages node)
  #:use-module (gnu packages password-utils)
  #:use-module (gnu packages perl)
  #:use-module (gnu packages php)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages ruby)
  #:use-module (gnu packages rust)
  #:use-module (gnu packages web)
  #:use-module (gnu packages xml)
  )

(define-public lang-packages
  (list
   bmon
   nmap
   tcpdump
   libpcap
   python-pip
   python-pylint
   python-proselint
   python-lsp-server
   aspell-dict-en
   aspell-dict-uk
   aspell
   go
   `(,rust "cargo")
   rust-clippy-0.0
   ruby
   ruby-rubocop
   ruby-yaml-lint
   cmake
   gawk
   gcc-toolchain
   llvm
   clang
   cppcheck
   ghc
   hlint
   tidy-html
   jq
   lua
   perl
   php
   libxml2
   guile-readline
   guile-colorized
   node
   ))

(define-public misc-packages
  (list
   sbcl-slynk
   ;;`(,bind "utils")
   ldns
   ndisc6
   fortune-mod
   pass-git-helper
   grive2
   ))
