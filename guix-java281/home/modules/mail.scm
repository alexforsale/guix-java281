(define-module (guix-java281 home modules mail)
  #:use-module (gnu home services)
  #:use-module (gnu home services shepherd)
  #:use-module (gnu services)
  #:use-module (gnu packages emacs-xyz)
  #:use-module (gnu packages mail)
  #:use-module (gnu packages python)
  #:use-module (guix build-system copy)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix gexp)
  #:use-module (guix packages))

(define-public mail-scripts
  (package
    (name "mail-scripts")
    (version "0.1")
    (source
     (local-file
      "../files/mail-scripts"
      #:recursive? #t))
    (build-system copy-build-system)
    (arguments
     `(#:install-plan
       '(("offlineimap-helper.py" "bin/offlineimap-helper.py"))))
    (propagated-inputs
     `(("python" ,python)))
    (home-page "https://github.com/alexforsale")
    (synopsis "Mail helper scripts")
    (description "Mail helper scripts")
    (license license:gpl3+)))

(define-public offlineimap-shepherd-service
  (shepherd-service
   (provision '(offlineimap3))
   (start #~(make-forkexec-constructor
             (list
              #$(file-append offlineimap3 "/bin/offlineimap"))))
   (stop #~(make-kill-destructor))))

(define alexforsale-afew
  (string-append
   "[SpamFilter]\n\n"
   "[KillThreadsFilter]\n\n"
   "[ArchiveSentMailsFilter]\n\n"
   "[SentMailsFilter]\n\n"
   "sent_tag = sent\n\n"
   "[FolderNameFilter]\n"
   "folder_lowercases = true\n"
   "maildir_separator = /\n\n"
   "[InboxFilter]\n\n"
   "[Filter.21]\n"
   "query = date:2021\n"
   "tags = +archive2021;-yahoo/archive;-gmail/archive;-hotmail/archive;-ymail/archive;-inbox;-yahoo/inbox;-gmail/inbox;-hotmail/inbox;-ymail/inbox\n"
   "message = tagged 2021-archive\n\n"
   "[Filter.20]\n"
   "query = date:2020\n"
   "tags = +archive2020;-yahoo/archive;-gmail/archive;-hotmail/archive;-ymail/archive;-inbox;-yahoo/inbox;-gmail/inbox;-hotmail/inbox;-ymail/inbox\n"
   "message = tagged 2020-archive\n\n"
   "[Filter.19]\n"
   "query = date:2019\n"
   "tags = +archive2019;-yahoo/archive;-gmail/archive;-hotmail/archive;-ymail/archive;-inbox;-yahoo/inbox;-gmail/inbox;-hotmail/inbox;-ymail/inbox\n"
   "message = tagged 2019-archive\n\n"
   "[Filter.18]\n"
   "query = date:2018\n"
   "tags = +archive2018;-yahoo/archive;-gmail/archive;-hotmail/archive;-ymail/archive;-inbox;-yahoo/inbox;-gmail/inbox;-hotmail/inbox;-ymail/inbox\n"
   "message = tagged 2018-archive\n\n"
   "[Filter.17]\n"
   "query = date:2017\n"
   "tags = +archive2017;-yahoo/archive;-gmail/archive;-hotmail/archive;-ymail/archive;-inbox;-yahoo/inbox;-gmail/inbox;-hotmail/inbox;-ymail/inbox\n"
   "message = tagged 2017-archive\n\n"
   "[Filter.16]\n"
   "query = date:2016\n"
   "tags = +archive2016;-yahoo/archive;-gmail/archive;-hotmail/archive;-ymail/archive;-inbox;-yahoo/inbox;-gmail/inbox;-hotmail/inbox;-ymail/inbox\n"
   "message = tagged 2016-archive\n\n"
   "[Filter.15]\n"
   "query = date:2015\n"
   "tags = +archive2015;-yahoo/archive;-gmail/archive;-hotmail/archive;-ymail/archive;-inbox;-yahoo/inbox;-gmail/inbox;-hotmail/inbox;-ymail/inbox\n"
   "message = tagged 2015-archive\n\n"
   "[Filter.14]\n"
   "query = date:2014\n"
   "tags = +archive2014;-yahoo/archive;-gmail/archive;-hotmail/archive;-ymail/archive;-inbox;-yahoo/inbox;-gmail/inbox;-hotmail/inbox;-ymail/inbox\n"
   "message = tagged 2014-archive\n\n"
   "[Filter.13]\n"
   "query = date:2013\n"
   "tags = +archive2013;-yahoo/archive;-gmail/archive;-hotmail/archive;-ymail/archive;-inbox;-yahoo/inbox;-gmail/inbox;-hotmail/inbox;-ymail/inbox\n"
   "message = tagged 2013-archive\n\n"
   "[Filter.12]\n"
   "query = date:2012\n"
   "tags = +archive2012;-yahoo/archive;-gmail/archive;-hotmail/archive;-ymail/archive;-inbox;-yahoo/inbox;-gmail/inbox;-hotmail/inbox;-ymail/inbox\n"
   "message = tagged 2012-archive\n\n"
   "[Filter.11]\n"
   "query = date:2011\n"
   "tags = +archive2011;-yahoo/archive;-gmail/archive;-hotmail/archive;-ymail/archive;-inbox;-yahoo/inbox;-gmail/inbox;-hotmail/inbox;-ymail/inbox\n"
   "message = tagged 2011-archive\n\n"
   "[Filter.10]\n"
   "query = date:2010\n"
   "tags = +archive2010;-yahoo/archive;-gmail/archive;-hotmail/archive;-ymail/archive;-inbox;-yahoo/inbox;-gmail/inbox;-hotmail/inbox;-ymail/inbox\n"
   "message = tagged 2010-archive\n\n"
   ))

(define alexforsale-msmtprc
  (string-append
   "defaults\n"
   "auth on\n"
   "tls on\n"
   "tls_trust_file /run/current-system/profile/etc/ssl/certs/ca-certificates.crt\n"
   "logfile ~/.local/var/log/msmtp.log\n\n"
   ;; gmail
   "account gmail\n"
   "host smtp.gmail.com\n"
   "port 587\n"
   "from alexarians@gmail.com\n"
   "user alexarians\n"
   "passwordeval \"pass google.com/app_pass/alexarians@gmail.com\"\n\n"
   ;; yahoo
   "account yahoo\n"
   "host smtp.mail.yahoo.com\n"
   "port 587\n"
   "from alexforsale@yahoo.com\n"
   "user alexforsale\n"
   "passwordeval \"pass login.yahoo.com/app_pass/alexforsale@yahoo.com\"\n\n"
   ;; hotmail
   "account hotmail\n"
   "auth on\n"
   "tls on\n"
   "host smtp-mail.outlook.com\n"
   "port 587\n"
   "from christian.alexander@windowslive.com\n"
   "user christian.alexander@windowslive.com\n"
   "passwordeval \"pass outlook.live.com/christian.alexander@windowslive.com\"\n\n"
   ;; ymail
   "account ymail\n"
   "host smtp.mail.yahoo.com\n"
   "port 587\n"
   "from christian.alexander@ymail.com\n"
   "user christian.alexander@ymail.com\n"
   "passwordeval \"login.yahoo.com/app_pass/christian.alexander@ymail.com\"\n\n"
   "account default : yahoo"
   ))

(define alexforsale-offlineimap
  (string-append
   ;; general
   "[general]\n"
   "accounts = gmail, hotmail, ymail, yahoo\n"
   "maxsyncaccounts = 1\n"
   "pythonfile = " (getenv "HOME") "/.guix-home/profile/bin/offlineimap-helper.py" "\n\n"
   ;; gmail
   "[Account gmail]\n"
   "localrepository = gmail-local\n"
   "remoterepository = gmail-remote\n"
   "synclabels = yes\n"
   "labelsheader = X-Keywords\n"
   "postsynchook = notmuch new --verbose\n\n"
   "[Repository gmail-local]\n"
   "Type = GmailMaildir\n"
   "localfolders = ~/.mail/gmail\n"
   "sync_deletes = yes\n"
   "nametrans = lambda f: re.sub('spam', '[Gmail]/Spam',\n"
   "                      re.sub('draft', '[Gmail]/Drafts',\n"
   "                      re.sub('inbox', 'INBOX',\n"
   "                      re.sub('sent', '[Gmail]/Sent Mail',\n"
   "                      re.sub('trash', '[Gmail]/Trash',"
   "                      re.sub('archive*', 'Archive',"
   "                      re.sub('archive$', '[Gmail]/All Mail', f)))))))\n\n"
   "[Repository gmail-remote]\n"
   "Type = Gmail\n"
   "remoteuser = alexarians@gmail.com\n"
   "remotepasseval = get_pass(\"gmail\")\n"
   "nametrans = lambda f: re.sub('.*All Mail$', 'archive',\n"
   "                      re.sub('^Archive', 'archive',\n"
   "                      re.sub('.*Drafts$', 'draft',\n"
   "                      re.sub('.*Spam$', 'spam',\n"
   "                      re.sub('.*Sent Mail$', 'sent',\n"
   "                      re.sub('.*Trash$', 'trash',\n"
   "                      re.sub('INBOX', 'inbox', f)))))))\n"
   "folderfilter = lambda foldername: foldername not in ['[Gmail]/Important', '[Gmail]/Starred']\n"
   "sslcacertfile = /run/current-system/profile/etc/ssl/certs/ca-certificates.crt\n"
   "ssl_version = tls1_2\n"
   "usecompression = yes\n\n"
   ;; yahoo
   "[Account yahoo]\n"
   "localrepository = yahoo-local\n"
   "remoterepository = yahoo-remote\n"
   "postsynchook = notmuch new --verbose\n\n"
   "[Repository yahoo-local]\n"
   "Type = Maildir\n"
   "localfolders = ~/.mail/yahoo\n"
   "sync_deletes = yes\n"
   "nametrans = lambda f: re.sub('spam', 'Bulk Mail',\n"
   "                      re.sub('draft', 'Draft',\n"
   "                      re.sub('inbox', 'Inbox',\n"
   "                      re.sub('sent', 'Sent',\n"
   "                      re.sub('trash', 'Trash',\n"
   "                      re.sub('archive', 'Archive', f))))))\n\n"
   "[Repository yahoo-remote]\n"
   "Type = IMAP\n"
   "remotehost = imap.mail.yahoo.com\n"
   "remoteuser = alexforsale@yahoo.com\n"
   "remotepasseval = get_pass(\"yahoo\")\n"
   "nametrans = lambda f: re.sub('.*Archive', 'archive',\n"
   "                      re.sub('.*Draft$', 'draft',\n"
   "                      re.sub('.*Bulk Mail$', 'spam',\n"
   "                      re.sub('.*Sent$', 'sent',\n"
   "                      re.sub('.*Trash$', 'trash',\n"
   "                      re.sub('Inbox', 'inbox', f))))))\n\n"
   "maxconnections = 1\n"
   "sslcacertfile = /run/current-system/profile/etc/ssl/certs/ca-certificates.crt\n"
   "ssl_version = tls1_2\n"
   "usecompression = no\n\n"
   ;; hotmail
   "[Account hotmail]\n"
   "localrepository = hotmail-local\n"
   "remoterepository = hotmail-remote\n"
   "postsynchook = notmuch new --verbose\n\n"
   "[Repository hotmail-local]\n"
   "Type = Maildir\n"
   "localfolders = ~/.mail/hotmail\n"
   "sync_deletes = yes\n"
   "nametrans = lambda f: re.sub('spam', 'Junk',\n"
   "                      re.sub('draft', 'Drafts',\n"
   "                      re.sub('inbox', 'Inbox',\n"
   "                      re.sub('sent', 'Sent',\n"
   "                      re.sub('trash', 'Deleted',\n"
   "                      re.sub('notes', 'Notes',\n"
   "                      re.sub('outbox', 'Outbox',\n"
   "                      re.sub('archive', 'Archive', f))))))))\n\n"
   "[Repository hotmail-remote]\n"
   "Type = IMAP\n"
   "remotehost = imap-mail.outlook.com\n"
   "remoteuser = christian.alexander@windowslive.com\n"
   "remotepasseval = get_pass(\"hotmail\")\n"
   "nametrans = lambda f: re.sub('.*Archive', 'archive',\n"
   "                      re.sub('.*Drafts$', 'draft',\n"
   "                      re.sub('.*Junk$', 'spam',\n"
   "                      re.sub('.*Sent$', 'sent',\n"
   "                      re.sub('.*Deleted$', 'trash',\n"
   "                      re.sub('.*Outbox$', 'outbox',\n"
   "                      re.sub('.*Notes$', 'notes',\n"
   "                      re.sub('Inbox', 'inbox', f))))))))\n\n"
   "maxconnections = 1\n"
   "sslcacertfile = /run/current-system/profile/etc/ssl/certs/ca-certificates.crt\n"
   "ssl_version = tls1_2\n"
   "usecompression = no\n\n"
   ;; ymail
   "[Account ymail]\n"
   "localrepository = ymail-local\n"
   "remoterepository = ymail-remote\n"
   "postsynchook = notmuch new --verbose\n\n"
   "[Repository ymail-local]\n"
   "Type = Maildir\n"
   "localfolders = ~/.mail/ymail\n"
   "sync_deletes = yes\n"
   "nametrans = lambda f: re.sub('spam', 'Bulk Mail',\n"
   "                      re.sub('draft', 'Draft',\n"
   "                      re.sub('inbox', 'Inbox',\n"
   "                      re.sub('sent', 'Sent',\n"
   "                      re.sub('trash', 'Trash',\n"
   "                      re.sub('archive', 'Archive', f))))))\n\n"
   "[Repository ymail-remote]\n"
   "Type = IMAP\n"
   "remotehost = imap.mail.yahoo.com\n"
   "remoteuser = christian.alexander@ymail.com\n"
   "remotepasseval = get_pass(\"ymail\")\n"
   "nametrans = lambda f: re.sub('.*Archive', 'archive',\n"
   "                      re.sub('.*Draft$', 'draft',\n"
   "                      re.sub('.*Bulk Mail$', 'spam',\n"
   "                      re.sub('.*Sent$', 'sent',\n"
   "                      re.sub('.*Trash$', 'trash',\n"
   "                      re.sub('Inbox', 'inbox', f))))))\n\n"
   "maxconnections = 1\n"
   "sslcacertfile = /run/current-system/profile/etc/ssl/certs/ca-certificates.crt\n"
   "ssl_version = tls1_2\n"
   "usecompression = no\n\n"
   ))

(define alexforsale-notmuch
  (string-append
   "[database]\n"
   "path=/home/alexforsale/.mail\n\n"
   "[user]\n"
   "primary_email=alexforsale@yahoo.com\n"
   "other_email=alexarians@gmail.com;christian.alexander@ymail.com;christian.alexander@windowslive.com\n"
   "[new]\n"
   "tags=new\n\n"
   "[search]\n\n"
   "[maildir]\n"
   ))

(define-public email-services
  (list
   (simple-service
    'msmtprc
    home-files-service-type
    `((".msmtprc"
       ,(plain-file "msmtprc" alexforsale-msmtprc))))
   (simple-service
    'offlineimaprc
    home-files-service-type
    `((".config/offlineimap/config"
       ,(plain-file "config" alexforsale-offlineimap))))
   (simple-service
    'notmuch-hook
    home-files-service-type
    `((".mail/.notmuch/hooks"
       ,(local-file "../files/notmuch-hooks" #:recursive? #t))))
   (simple-service
    'afew
    home-files-service-type
    `((".config/afew/config"
       ,(plain-file "config" alexforsale-afew))))
   (simple-service
    'notmuch-config
    home-files-service-type
    `((".notmuch-config"
       ,(plain-file "notmuch-config" alexforsale-notmuch))))))

(define-public mail-packages
  (list
   offlineimap3
   notmuch
   afew
   msmtp
   mail-scripts
   emacs-gnus-alias))
