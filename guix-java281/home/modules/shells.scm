(define-module (guix-java281 home modules shells)
  #:use-module (gnu packages)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages haskell-apps)
  #:use-module (gnu packages shells)
  #:use-module (gnu packages shellutils)
  #:use-module (gnu services)
  #:use-module (gnu home services)
  #:use-module (gnu home services shells)
  #:use-module (guix gexp))

(define-public zsh-packages
  (list
   zsh
   zsh-autosuggestions
   zsh-syntax-highlighting
   shellcheck))

(define-public bash-packages
  (list
   bash bash-completion))

(define-public bash-services
  (list
   (service home-bash-service-type
            (home-bash-configuration
             (guix-defaults? #f)
             (environment-variables
              `(("LESSHISTFILE" . "$XDG_CACHE_HOME/.lesshst")
                ("SHELL" . ,(file-append bash "/bin/bash"))
                ("BASH_DIR" . "$XDG_CONFIG_HOME/bash.d")
                ("BASH_DATA" . "$XDG_DATA_HOME/bash")))
             (bashrc
              (list
               (local-file "../files/bashrc")))
             (bash-profile
              (list
               (local-file "../files/bash_profile")))
             (bash-logout
              (list
               (local-file "../files/bash_logout")))))
   (simple-service
    'bash-d
    home-files-service-type
    (list `(".config/bash.d"
            ,(local-file "../files/bash.d" #:recursive? #t))))))

(define-public zsh-services
  (list
   (service home-zsh-service-type
            (home-zsh-configuration
             (xdg-flavor? #t)
             (environment-variables
              `(("LESSHISTFILE" . "$XDG_CACHE_HOME/.lesshst")
                ("SHELL" . ,(file-append zsh "/bin/zsh"))))
             (zshrc
              (list
               (local-file "../files/zshrc")))
             (zprofile
              (list
               (local-file "../files/zprofile")))
             (zlogin
              (list
               (local-file "../files/zlogin")))
             (zlogout
              (list
               (local-file "../files/zlogout")))))
   (simple-service
    'zsh-d
    home-files-service-type
    (list `(".config/zsh/conf.d"
            ,(local-file "../files/zsh.d" #:recursive? #t))))))

(define-public modular-profile-services
  (list
   (simple-service
    'modular-profile
    home-shell-profile-service-type
    (list
     (plain-file
      "local-bin"
      "\

if [ -d \"${HOME}/bin\" ] ; then
    export PATH=\"${HOME}/bin:${PATH}\"
fi
")
     (plain-file
      "modular"
      "\
if [ -d \"${HOME}/.config/profile.d\" ]; then
    for profile in \"${HOME}\"/.config/profile.d/*.sh; do
        . \"${profile}\"
    done
    unset profile
fi

")
     (plain-file
      "cleanup-path"
      "\
if [ -n \"${PATH}\" ]; then
    old_PATH=${PATH}:; PATH=
    while [ -n \"${old_PATH}\" ]; do
        x=${old_PATH%%:*}       # the first remaining entry
        case ${PATH}: in
            *:\"$x\":*) ;;         # already there
            *) PATH=${PATH}:$x;;    # not there yet
        esac
        old_PATH=${old_PATH#*:}
    done
    PATH=${PATH#:}
    unset old_PATH x
fi
")
     (plain-file
      "local-profile"
      "\
# local ~/.profile
if [ -r \"${HOME}\"/.config/profile.local ];then
   . \"${HOME}\"/.config/profile.local
   elif [ -r \"${HOME}\"/.profile.local ];then
        . \"${HOME}\"/.profile.local
   fi
")))))

(define-public modular-profile-extensions
  (list
   (simple-service
    'profile-d
    home-files-service-type
    (list `(".config/profile.d"
            ,(local-file "../files/profile.d" #:recursive? #t))))))

(define-public global-variables
  (list
   (simple-service
    'global
    home-environment-variables-service-type
    `(("PATH" . "${PATH}:${HOME}/.local/bin:/bin:/usr/bin")))))
