(define-module (guix-java281 home modules fonts)
  #:use-module (gnu home services)
  #:use-module (gnu home services fontutils)
  #:use-module (gnu packages)
  #:use-module (gnu packages fonts)
  #:use-module (gnu packages fontutils)
  #:use-module (gnu packages xorg)
  #:use-module (gnu services)
  #:use-module (guix gexp))

(define-public fonts-packages
  (list
   font-adobe-source-sans-pro
   font-adobe-source-serif-pro
   font-adobe-source-code-pro
   font-fira-code
   font-fira-mono
   font-google-roboto
   font-inconsolata
   font-borg-sans-mono
   font-fantasque-sans
   font-iosevka
   font-awesome
   ;;fontconfig
   xlsfonts
   ))

(define-public font-services
  (list
   (simple-service
    'autohint
    home-files-service-type
    `((".config/fontconfig/conf.d/99-autohint.conf"
       ,(plain-file
         "autohint"
         "\
<?xml version=\"1.0\"?>
<!DOCTYPE fontconfig SYSTEM \"urn:fontconfig:fonts.dtd\">
<fontconfig>
  <match target=\"font\">
    <edit name=\"autohint\" mode=\"assign\">
      <bool>true</bool>
    </edit>
  </match>
</fontconfig>"))
      (".config/fontconfig/conf.d/99-hintstyle.conf"
       ,(plain-file
         "hintstyle"
         "\
<?xml version=\"1.0\"?>
<!DOCTYPE fontconfig SYSTEM \"urn:fontconfig:fonts.dtd\">
<fontconfig>
  <match target=\"font\">
    <edit name=\"hintstyle\" mode=\"assign\">
      <const>hintfull</const>
    </edit>
  </match>
</fontconfig>"))
      (".config/fontconfig/conf.d/99-pixel-alignment.conf"
       ,(plain-file
         "pixel"
         "\
<?xml version=\"1.0\"?>
<!DOCTYPE fontconfig SYSTEM \"urn:fontconfig:fonts.dtd\">
<fontconfig>
  <match target=\"font\">
    <edit name=\"rgba\" mode=\"assign\">
      <const>rgb</const>
    </edit>
  </match>
</fontconfig>"))
      (".config/fontconfig/conf.d/99-lcdfilter.conf"
       ,(plain-file
         "lcdfilter"
         "\
<?xml version=\"1.0\"?>
<!DOCTYPE fontconfig SYSTEM \"urn:fontconfig:fonts.dtd\">
<fontconfig>
  <match target=\"font\">
    <edit name=\"lcdfilter\" mode=\"assign\">
      <const>lcddefault</const>
    </edit>
  </match>
</fontconfig>"))
      (".config/fontconfig/conf.d/99-fallback.conf"
       ,(plain-file
         "fallback"
         "\
<fontconfig>
 <alias>
  <family>sans-serif</family>
  <prefer>
   <family>Noto Sans</family>
   <family>Open Sans</family>
   <family>Droid Sans</family>
   <family>Ubuntu</family>
   <family>Roboto</family>
   <family>Fantasque Sans</family>
   <family>Source Sans</family>
  </prefer>
 </alias>
 <alias>
  <family>serif</family>
  <prefer>
   <family>Noto Serif</family>
   <family>Droid Serif</family>
   <family>Roboto Slab</family>
   <family>Source Serif Pro</family>
  </prefer>
 </alias>
 <alias>
  <family>monospace</family>
  <prefer>
   <family>Noto Sans Mono</family>
   <family>Inconsolatazi4</family>
   <family>Ubuntu Mono</family>
   <family>Droid Sans Mono</family>
   <family>Roboto Mono</family>
   <family>Source Code</family>
  </prefer>
 </alias>
</fontconfig>"))))))
