(define-module (guix-java281 home modules desktop)
  #:use-module (gnu home services)
  #:use-module (gnu home services desktop)
  #:use-module (gnu home services shepherd)
  #:use-module (gnu home services xdg)
  #:use-module (gnu services)
  #:use-module (gnu packages)
  #:use-module (gnu packages emacs-xyz)
  #:use-module (gnu packages freedesktop)
  #:use-module (gnu packages gnome)
  #:use-module (gnu packages gnome-xyz)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages graphviz)
  #:use-module (gnu packages gimp)
  #:use-module (gnu packages inkscape)
  #:use-module (gnu packages libcanberra)
  #:use-module (gnu packages libreoffice)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages messaging)
  #:use-module (gnu packages terminals)
  #:use-module (gnu packages xdisorg)
  #:use-module (gnu packages xfce)
  #:use-module (gnu packages xorg)
  #:use-module (guix gexp))

(define-public desktop-packages
  (list
   acpi
   brightnessctl
   gnome-keyring
   `(,gtk+ "bin")
   xprop
   xwininfo
   xsetroot
   xcursor-themes
   xmodmap
   xsettingsd
   libcanberra
   libvterm
   redshift
   setxkbmap
   sound-theme-freedesktop
   thunar
   thunar-volman
   wmctrl
   xdg-utils
   xdot
   papirus-icon-theme
   materia-theme
   ;;
   libreoffice
   gimp
   inkscape
   tdlib
   emacs-telega
   emacs-telega-server
   emacs-telega-contrib
   ))

(define-public desktop-services
  (list
   (service
    home-redshift-service-type
    (home-redshift-configuration
     (location-provider 'geoclue2)))
   (service
    home-xdg-mime-applications-service-type
    (home-xdg-mime-applications-configuration
     (default '((x-scheme-handler/http . firefox.desktop)
                (x-scheme-handler/https . firefox.desktop)
                (x-scheme-handler/chrome . firefox.desktop)
                (text/html . firefox.desktop)
                (application/x-extension-htm . firefox.desktop)
                (application/x-extension-html . firefox.desktop)
                (application/x-extension-shtml . firefox.desktop)
                (application/xhtml+xml . firefox.desktop)
                (application/x-extension-xhtml . firefox.desktop)
                (application/x-extension-xht . firefox.desktop)
                (x-scheme-handler/org-protocol . org-protocol.desktop)))
     (added '((inode/directory . thunar.desktop)
              (x-scheme-handler/mailto . emacsclient-mail.desktop)))
     (desktop-entries
      (list
       (xdg-desktop-entry
        (file "org-protocol")
        (name "Org-Protocol")
        (type 'applications)
        (config
         '((exec . "emacsclient %u"))))))))))

(define-public xsettingsd-shepherd-service
  (shepherd-service
   (provision '(xsettingsd))
   (start #~(make-forkexec-constructor
             (list
              #$(file-append xsettingsd "/bin/xsettingsd"))))
   (stop #~(make-kill-destructor))))

(define-public xsettingsd-configuration
  (list
   (simple-service
    'xsettingsd
    home-files-service-type
    `((".config/xsettingsd/xsettingsd.conf"
       ,(local-file "../files/xsettingsd.conf"))))))

(define-public gtk-configuration
  (list
   (simple-service
    'gtkpath
    home-environment-variables-service-type
    '(("GTK_RC_FILES" . "${XDG_CONFIG_HOME}/gtk-1.0/gtkrc")
      ("GTK2_RC_FILES" . "${XDG_CONFIG_HOME}/gtk-2.0/gtkrc")))
   (simple-service
    'gtkrc
    home-files-service-type
    `((".config/gtk-3.0/settings.ini"
       ,(local-file "../files/gtk3rc"))
      (".config/gtk-2.0/gtkrc"
       ,(local-file "../files/gtk2rc"))))))

(define-public rofi-configuration
  (list
   (simple-service
    'rofi
    home-files-service-type
    `((".config/rofi/config.rasi"
       ,(local-file "../files/rofi.rasi"))
      (".config/rofi/themes"
       ,(local-file "../files/rofi.themes" #:recursive? #t))))))

(define-public xorg-user-configuration
  (list
   (simple-service
    'Xconfig
    home-files-service-type
    `((".Xresources"
       ,(local-file "../files/Xresources"))
      (".config/Xresources.d/URxvt"
       ,(local-file "../files/URxvt.Xresources"))
      (".config/Xresources.d/xterm"
       ,(local-file "../files/xterm.Xresources"))
      ;; TODO: find a way to make the symlink
      ;; executable
      ;;(".xsession"
      ;; ,(local-file "../files/xsession"))
      (".xprofile"
       ,(local-file "../files/xprofile"))
      (".config/xprofile.d"
       ,(local-file "../files/xprofile.d"
                    #:recursive? #t))))))
