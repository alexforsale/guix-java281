(define-module (guix-java281 home modules emacs)
  #:use-module (gnu home services)
  #:use-module (gnu home services shepherd)
  #:use-module (gnu packages)
  #:use-module (gnu packages admin)
  #:use-module (gnu packages emacs)
  #:use-module (gnu packages emacs-xyz)
  #:use-module (gnu packages mail)
  #:use-module (gnu services)
  #:use-module (guix gexp)
  #:use-module (guix-java281 packages emacs))

(define-public emacs-packages
  (list
   emacs-selectric-mode
   emacs-evil-embrace
   emacs-evil-easymotion
   emacs-ansible
   emacs-block-nav
   emacs-consult-flycheck
   emacs-consult-yasnippet
   emacs-consult-projectile
   emacs-consult-lsp
   emacs-consult-org-roam
   emacs-symbol-overlay
   emacs-emojify
   emacs-vertico-posframe
   emacs-treemacs
   emacs-treemacs-extra
   emacs-perspective
   emacs-geiser-guile
   emacs-treemacs-extra
   emacs-org-contrib
   emacs-restart-emacs
   emacs-ripgrep
   emacs-sly
   git-modes
   emacs-lsp-treemacs
   emacs-lsp-ui
   emacs-lsp-mode
   emacs-password-store-otp
   emacs-pass
   emacs-aggressive-indent
   emacs-org-journal
   emacs-elfeed-org
   emacs-org-roam-bibtex
   emacs-org-ref
   emacs-docker
   emacs-docker-compose-mode
   emacs-dockerfile-mode
   emacs-evil-org
   emacs-evil-escape
   emacs-evil-collection
   emacs-general
   emacs-jinja2-mode
   emacs-ansible-doc
   emacs-rust-mode
   emacs-undo-fu-session
   emacs-undo-fu
   emacs-visual-regexp
   emacs-markdown-preview-mode
   emacs-markdown-mode
   emacs-nginx-mode
   emacs-nix-mode
   emacs-toml-mode
   emacs-yaml-mode
   emacs-yaml
   emacs-yasnippet-snippets
   emacs-yasnippet
   emacs-multiple-cursors
   emacs-diff-hl
   emacs-hl-todo
   emacs-rainbow-identifiers
   emacs-rainbow-delimiters
   emacs-rainbow-mode
   emacs-ace-window
   emacs-vterm-toggle
   emacs-flycheck-guile
   emacs-flycheck
   emacs-diminish
   emacs-helpful
   emacs-elisp-demos
   emacs-geiser
   emacs-kind-icon
   emacs-cape
   emacs-corfu-doc
   emacs-corfu
   emacs-embark
   emacs-consult-dir
   emacs-consult
   emacs-marginalia
   emacs-orderless
   emacs-vertico
   emacs-theme-magic
   emacs-modus-themes
   emacs-doom-themes
   emacs-doom-modeline
   emacs-all-the-icons-completion
   emacs-all-the-icons-dired
   emacs-all-the-icons-ibuffer
   emacs-all-the-icons
   emacs-org
   emacs-guix
   emacs-which-key
   emacs-no-littering
   emacs-tempel
   emacs-notmuch
   emacs-consult-notmuch
   emacs-perspective-exwm
   ))

(define-public emacs-shepherd-service
  (shepherd-service
   (provision '(emacs))
   (start #~(make-forkexec-constructor
             (list
              #$(file-append emacs "/bin/emacs")
              "--fg-daemon")))
   (stop #~(make-system-destructor
            (string-join
             (list
              (string-append #$emacs "/bin/emacs")
              "--eval"
              "\"(kill-emacs)\""))))))

(define-public emacs-configuration
  (list
   (simple-service
    'load-path
    home-environment-variables-service-type
    '(("EMACSLOADPATH" . "${HOME}/.guix-home/profile/share/emacs/site-lisp:${EMACSLOADPATH}")))))
