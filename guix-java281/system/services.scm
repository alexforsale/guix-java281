(define-module (guix-java281 system services)
  ;; #:use-module (gnu services avahi)
  ;; #:use-module (gnu services authentication)
  ;; #:use-module (gnu services cups)
  ;; #:use-module (gnu services desktop)
  ;; #:use-module (gnu services mcron)
  ;; #:use-module (gnu services networking)
  ;; #:use-module (gnu services spice)
  ;; #:use-module (gnu services xorg)
  ;; #:use-module (srfi srfi-1)
  #:use-module (guix gexp)
  ;; #:use-module (gnu)
  #:use-module (gnu bootloader)
  #:use-module (gnu bootloader grub)
  #:use-module (gnu system)
  #:use-module (gnu system accounts)
  #:use-module (gnu system file-systems)
  #:use-module (gnu system keyboard)
  #:use-module (gnu system shadow)
  #:use-module (gnu system nss)
  #:use-module (gnu services)
  #:use-module (gnu services base)
  #:use-module (gnu services cups)
  #:use-module (gnu services desktop)
  #:use-module (gnu services linux)
  #:use-module (gnu services mcron)
  #:use-module (gnu services nix)
  #:use-module (gnu services ssh)
  #:use-module (gnu services xorg)
  #:use-module (gnu packages)
  #:use-module (gnu packages admin)
  #:use-module (gnu packages audio)
  #:use-module (gnu packages base)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages certs)
  #:use-module (gnu packages compton)
  #:use-module (gnu packages curl)
  #:use-module (gnu packages dunst)
  #:use-module (gnu packages emacs)
  #:use-module (gnu packages emacs-xyz)
  #:use-module (gnu packages file)
  #:use-module (gnu packages fonts)
  #:use-module (gnu packages freedesktop)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages gnome)
  #:use-module (gnu packages gnome-xyz)
  #:use-module (gnu packages gnupg)
  #:use-module (gnu packages imagemagick)
  #:use-module (gnu packages image-viewers)
  #:use-module (gnu packages libusb)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages lisp)
  #:use-module (gnu packages lsof)
  #:use-module (gnu packages lxde)
  #:use-module (gnu packages music)
  #:use-module (gnu packages ncurses)
  #:use-module (gnu packages nfs)
  #:use-module (gnu packages package-management)
  #:use-module (gnu packages password-utils)
  #:use-module (gnu packages pulseaudio)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages rsync)
  #:use-module (gnu packages rust-apps)
  #:use-module (gnu packages screen)
  #:use-module (gnu packages shells)
  #:use-module (gnu packages shellutils)
  #:use-module (gnu packages suckless)
  #:use-module (gnu packages terminals)
  #:use-module (gnu packages version-control)
  #:use-module (gnu packages vim)
  #:use-module (gnu packages wget)
  #:use-module (gnu packages wm)
  #:use-module (gnu packages xdisorg)
  #:use-module (gnu packages xorg)
  #:use-module (nongnu packages linux)
  #:use-module (nongnu packages vpn)
  #:use-module (nongnu services vpn)
  #:use-module (nongnu system linux-initrd)
  )

(define-public %xorg-libinput-config
  "Section \"InputClass\"
        Identifier \"Touchpads\"
        Driver \"libinput\"
        MatchDevicePath \"/dev/input/event*\"
        MatchIsTouchpad \"on\"

        Option \"Tapping\" \"on\"
        Option \"TappingDrag\" \"on\"
        Option \"DisableWhileTyping\" \"on\"
        Option \"MiddleEmulation\" \"on\"
        Option \"NaturalScrolling\" \"on\"
      EndSection
      Section \"InputClass\"
        Identifier \"Keyboards\"
        Driver \"libinput\"
        MatchDevicePath \"/dev/input/event*\"
        MatchIsKeyboard \"on\"
      EndSection
      ")

(define-public %backlight-udev-rule
  (udev-rule
   "90-backlight.rules"
   (string-append "ACTION==\"add\", SUBSYSTEM==\"backlight\", "
                  "RUN+=\"/run/current-system/profile/bin/chgrp video /sys/class/backlight/%k/brightness\""
                  "\n"
                  "ACTION==\"add\", SUBSYSTEM==\"backlight\", "
                  "RUN+=\"/run/current-system/profile/bin/chmod g+w /sys/class/backlight/%k/brightness\"")))

(define-public %java281-local-sudoers-file
  (plain-file "sudoers" "\
Defaults env_keep += \"LANG LANGUAGE LINGUAS LC_* _XKB_CHARSET\"
Defaults env_keep += \"HOME\"
Defaults env_keep += \"XAPPLRESDIR XFILESEARCHPATH XUSERFILESEARCHPATH\"
Defaults env_keep += \"QTDIR KDEDIR\"
Defaults env_keep += \"XDG_SESSION_COOKIE\"
Defaults mail_badpass
Defaults maxseq = 1000
root ALL=(ALL) ALL
%wheel ALL=(ALL) NOPASSWD: ALL
  "))

(define-public %gc-job-system
  ;; Collect garbage 5 minutes after midnight every day.
  ;; use vixie syntax since I still figuring out
  ;; mcron
  #~(job "5 0 * * *"
         "guix system delete-generations 1w"))

(define-public %gc-job-admin-local
  ;; same as system only for this user.
  #~(job "5 0 * * *"
         "guix system delete-generations 1w"
         #:user "admin-local"))

(define-public %java281-local-groups
  (cons*
   (user-group
    (name "alexforsale")
    (id 1000))
   (user-group
    (name "realtime")
    (system? #t))
   (user-group
    (name "admin-local")
    (id 1100))
   (user-group
    (name "network-manager")
    (system? #t))
   %base-groups))

(define-public %java281-local-users
  (cons*
   (user-account
    (name "admin-local")
    (comment "Local Administrator")
    (uid 1100)
    (group "admin-local")
    (supplementary-groups '("wheel" "netdev" "users" "input" "lp"
                            "audio" "video" "tty" "network-manager")))
   %base-user-accounts))

(define-public %java281-common-nongnu-services
  (list
   (zerotier-one-service)))

(define-public %java281-common-services
  (list
   (extra-special-file "/bin/ls" (file-append coreutils "/bin/ls"))
   (extra-special-file "/usr/bin/gpg2" (file-append gnupg "/bin/gpg"))
   (extra-special-file "/usr/bin/python" (file-append python "/bin/python3"))
   (service earlyoom-service-type)
   (service nix-service-type)
   (service openssh-service-type
            (openssh-configuration
             (permit-root-login 'prohibit-password)
             ;;(permit-root-login 'without-password) ;; earlier version of guix still uses this
             (authorized-keys
              `(("rsa" ,(local-file "files/id_rsa.pub"))
                ("id_ed25519" ,(local-file "files/id_ed25519.pub"))))))
   (simple-service
    'java281-cron-jobs
    mcron-service-type
    (list %gc-job-admin-local
          %gc-job-system))))

(define-public %java281-common-desktop-services
  (append
   (list
    (service cups-service-type)
    (service gnome-keyring-service-type))
   (modify-services
       %desktop-services
     (delete gdm-service-type)
     (guix-service-type config =>
                        (guix-configuration
                         (inherit config)
                         (substitute-urls
                          (append (list "https://substitutes.nonguix.org")
                                  %default-substitute-urls))
                         (authorized-keys
                          (append
                           (list
                            (plain-file "non-guix.pub"
                                        "\
(public-key
 (ecc
  (curve Ed25519)
  (q #C1FD53E5D4CE971933EC50C9F307AE2171A2D3B52C804642A7A35F84F3A4EA98#)))"))
                           %default-authorized-guix-keys))))
     (udev-service-type
      config =>
      (udev-configuration
       (inherit config)
       (rules (cons %backlight-udev-rule
                    (udev-configuration-rules config)))))
     (geoclue-service
      config =>
      #:applications
      (append
       "redshift"
       %standard-geoclue-applications))
     )))

(define-public %java281-basic-packages
  (append
   (list
    nss-certs git vim emacs
    screen gnupg pinentry pinentry-tty
    htop neofetch tree nfs-utils ntfs-3g
    file libmtp btrfs-progs efibootmgr
    inetutils lsof rsync stow strace
    wget curl acpi)
   %base-packages))

(define-public %exwm-packages
  (list
   emacs-exwm
   emacs-desktop-environment))

(define-public %i3-packages
  (list
   i3-wm i3status perl-anyevent-i3 i3lock
   ))

(define-public %stumpwm-packages
  (list
   sbcl stumpwm `(,stumpwm "lib")
   sbcl-stumpwm-ttf-fonts font-dejavu
   sbcl-stumpwm-winner-mode sbcl-stumpwm-net
   sbcl-stumpwm-mem sbcl-stumpwm-cpu
   sbcl-stumpwm-globalwindows ;; stumpwm-with-slynk
   sbcl-stumpwm-swm-gaps sbcl-stumpwm-screenshot
   sbcl-stumpwm-pass stumpish sbcl-stumpwm-wifi
   sbcl-stumpwm-stumptray
   ))

(define-public %zsh-packages
  (list zsh zsh-syntax-highlighting zsh-autosuggestions))

(define-public %bash-packages
  (list
   bash bash-completion))

(define-public %java281-desktop-packages
  (list
   i3status dmenu xrandr picom feh udiskie
   dunst unclutter-xfixes xset xsetroot xprop xrdb
   xsettingsd xclip xsel mkfontdir mkfontscale xwininfo
   xsetroot setxkbmap python-pywal rofi adwaita-icon-theme
   gnome-themes-extra papirus-icon-theme materia-theme
   xterm brightnessctl xss-lock libnotify wmctrl libsecret
   password-store pass-otp xdotool lxappearance
   fd ripgrep pulseaudio pavucontrol playerctl
   imagemagick alsa-utils alsa-plugins `(,alsa-plugins "pulseaudio")
   `(,alsa-plugins "jack") pamixer glib shared-mime-info
   bluez bluez-alsa python libvterm ncurses
   xf86-input-libinput redshift
   ))

(define-public %java281-base-operating-system
  (operating-system
    (host-name "java281")
    (timezone "Asia/Jakarta")
    (locale "en_US.utf8")

    (keyboard-layout (keyboard-layout "us" "altgr-intl"))

    (bootloader (bootloader-configuration
                 (bootloader grub-efi-bootloader)
                 (targets (list "/boot/efi"))
                 (keyboard-layout keyboard-layout)))

    ;; Guix doesn't like it when there isn't a file-systems
    ;; entry, so add one that is meant to be overridden
    (file-systems (cons*
                   (file-system
                     (mount-point "/tmp")
                     (device "none")
                     (type "tmpfs")
                     (check? #f))
                   %base-file-systems))

    ;; sudoers-file
    (sudoers-file %java281-local-sudoers-file)
    ;; local group
    (groups %java281-local-groups)
    ;; local user, we'll use LDAP (hopefully)
    (users %java281-local-users)

    (name-service-switch %mdns-host-lookup-nss)))
