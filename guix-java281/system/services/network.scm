(define-module (guix-java281 system services network)
  #:use-module (gnu packages admin)
  #:use-module (gnu services)
  #:use-module (gnu services admin)
  #:use-module (gnu services shepherd)
  #:use-module (guix gexp)
  #:use-module (guix records)
  #:use-module (ice-9 match)
  #:export (radvd-configuration
            radvd-configuration?
            radvd-configuration-package
            radvd-configuration-config-file
            radvd-service-type)
  )

(define-record-type* <radvd-configuration>
  radvd-configuration make-radvd-configuration radvd-configuration?
  (package radvd-configuration-package
           (default radvd))
  (config-file radvd-configuration-config-file
               (default "/etc/radvd.conf")))

(define radvd-shepherd-service
  (match-lambda
    (($ <radvd-configuration> package config-file)
     (let ((radvd (file-append package "/sbin/radvd")))
       (list (shepherd-service
              (documentation "router advertisement daemon for IPv6.")
              (provision '(radvd))
              (requirement '(networking))
              (start #~(make-forkexec-constructor
                        (list #$radvd "-n" "--config"
                              #$config-file)))
              (stop #~(make-kill-destructor))))))))

(define radvd-service-type
  (service-type
   (name 'radvd)
   (extensions
    (list (service-extension shepherd-root-service-type
                             radvd-shepherd-service)))
   (description
    "router advertisement daemon for IPv6.")))
