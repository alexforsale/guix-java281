(define-module (guix-java281 burundi system-configuration)
  #:use-module (guix-java281 system services)
  #:use-module (gnu bootloader)
  #:use-module (gnu bootloader grub)
  #:use-module (guix gexp)
  #:use-module (gnu packages)
  #:use-module (gnu packages fonts)
  #:use-module (gnu packages gnome)
  #:use-module (gnu packages shells)
  #:use-module (gnu packages lisp)
  #:use-module (gnu packages wm)
  #:use-module (gnu packages xorg)
  #:use-module (gnu packages xdisorg)
  #:use-module (gnu services)
  #:use-module (gnu services base)
  #:use-module (gnu services cups)
  #:use-module (gnu services desktop)
  #:use-module (gnu services mcron)
  #:use-module (gnu services networking)
  #:use-module (gnu services pm)
  #:use-module (gnu services sddm)
  #:use-module (gnu services sound)
  #:use-module (gnu services sysctl)
  #:use-module (gnu services xorg)
  #:use-module (gnu system)
  #:use-module (gnu system accounts)
  #:use-module (gnu system file-systems)
  #:use-module (gnu system keyboard)
  #:use-module (gnu system shadow)
  #:use-module (gnu system nss)
  #:use-module (nongnu packages linux)
  #:use-module (nongnu system linux-initrd)
  #:use-module (srfi srfi-1))

(define %java281-burundi-services
  (modify-services
      %java281-common-desktop-services
    (sysctl-service-type config =>
                         (sysctl-configuration
                          (settings (append '(
                                              ("vm.swappiness" . "30"))
                                            %default-sysctl-settings))))
    (elogind-service-type config =>
                          (elogind-configuration (inherit config)
                                                 (handle-lid-switch-external-power 'suspend)))
    ))

(operating-system
  (inherit %java281-base-operating-system)
  (host-name "burundi")
  (kernel linux)
  (initrd microcode-initrd)
  (firmware (list linux-firmware))
  (keyboard-layout (keyboard-layout "us" "altgr-intl"))
  (file-systems
   (cons*
    ;; root
    (file-system
      (device
       (uuid "71830682-d9de-4fe3-983a-dd83ede2c2a7"
             'btrfs))
      (mount-point "/")
      (type "btrfs")
      (options "subvol=@guix,compress=zstd"))
    ;; home
    (file-system
      (device
       (uuid "71830682-d9de-4fe3-983a-dd83ede2c2a7"
             'btrfs))
      (mount-point "/home")
      (type "btrfs")
      (options "subvol=@guix-home,compress=zstd"))
    ;; swap
    (file-system
      (device
       (uuid "71830682-d9de-4fe3-983a-dd83ede2c2a7"
             'btrfs))
      (mount-point "/swap")
      (type "btrfs")
      (options "subvol=@swap"))
    ;; efi
    (file-system
      (mount-point "/boot/efi")
      (device (uuid "81B3-1CE6" 'fat32))
      (type "vfat"))
    ;; google-drive
    (file-system
      (device
       (uuid "71830682-d9de-4fe3-983a-dd83ede2c2a7"
             'btrfs))
      (mount-point "/home/alexforsale/Documents/google-drive")
      (type "btrfs")
      (options "rw,relatime,uid=1000,gid=1000,subvol=@google-drive,compress=zstd"))
    ;; emacs.default
    (file-system
      (device
       (uuid "71830682-d9de-4fe3-983a-dd83ede2c2a7"
             'btrfs))
      (mount-point "/home/alexforsale/.config/emacs.default")
      (type "btrfs")
      (options "rw,relatime,uid=1000,gid=1000,subvol=@emacs.default,compress=zstd"))
    %base-file-systems))
  (swap-devices
   (list
    (swap-space
     (target "/swap/swapfile")
     (dependencies
      (filter
       (file-system-mount-point-predicate "/swap") file-systems)))))
  (groups %java281-local-groups)
  (users
   (cons*
    (user-account
     (name "alexforsale")
     (comment "Kristian Alexander P")
     (uid 1000)
     (home-directory "/home/alexforsale")
     (group "alexforsale")
     (supplementary-groups '("wheel" "netdev" "users" "realtime"
                             "input" "lp" "audio" "video"
                             "network-manager")))
    %java281-local-users))
  (packages
   (append
    %java281-desktop-packages
    %i3-packages
    %exwm-packages
    %stumpwm-packages
    %bash-packages
    %java281-basic-packages))
  (services
   (append
    (list
     (service
      slim-service-type
      (slim-configuration
       (xorg-configuration
        (xorg-configuration
         (modules (cons xf86-video-intel
                        %default-xorg-modules))
         (keyboard-layout keyboard-layout)
         (extra-config (list %xorg-libinput-config))))))
     (service
      tlp-service-type
      (tlp-configuration
       (cpu-scaling-governor-on-ac '("powersave"))
       (cpu-scaling-governor-on-bat '("powersave"))
       (cpu-scaling-min-freq-on-ac 933000)
       (cpu-scaling-min-freq-on-bat 933000)
       (cpu-scaling-max-freq-on-ac 1999000)
       (cpu-scaling-max-freq-on-bat 1866000)
       (sched-powersave-on-ac? #t)
       (nmi-watchdog? #t)
       (restore-device-state-on-startup? #t))))
    %java281-common-nongnu-services
    %java281-common-services
    %java281-burundi-services))
  (name-service-switch %mdns-host-lookup-nss))
