(define-module (guix-java281 senegal system-configuration)
  #:use-module (guix-java281 system services)
  #:use-module (guix gexp)
  #:use-module (gnu system)
  #:use-module (gnu system accounts)
  #:use-module (gnu system file-systems)
  #:use-module (gnu system keyboard)
  #:use-module (gnu system shadow)
  #:use-module (gnu system nss)
  #:use-module (gnu services)
  #:use-module (gnu services mcron)
  #:use-module (gnu services sddm)
  #:use-module (gnu services spice)
  #:use-module (gnu services sysctl)
  #:use-module (gnu services xorg)
  #:use-module (gnu packages)
  #:use-module (gnu packages shells)
  #:use-module (gnu packages xorg)
  #:use-module (nongnu packages linux)
  #:use-module (nongnu system linux-initrd)
  #:use-module (srfi srfi-1))

(define %java281-senegal-services
  (modify-services
      %java281-common-desktop-services
    (sysctl-service-type config =>
                         (sysctl-configuration
                          (settings (append '(
                                              ("vm.swappiness" . "30"))
                                            %default-sysctl-settings))))))

(operating-system
  (inherit %java281-base-operating-system)
  (host-name "senegal")
  (keyboard-layout (keyboard-layout "us" "altgr-intl"))
  (kernel linux)
  (initrd microcode-initrd)
  (firmware (list linux-firmware))
  (file-systems
   (cons*
    (file-system
      (device (file-system-label "EFI"))
      (mount-point "/boot/efi")
      (type "vfat"))
    (file-system
      (device
       (file-system-label "root"))
      (mount-point "/")
      (type "btrfs")
      (options "subvol=guix,compress=zstd"))
    (file-system
      (device
       (file-system-label "root"))
      (mount-point "/home")
      (type "btrfs")
      (options "subvol=guix-home,compress=zstd"))
    %base-file-systems))
  (swap-devices
   (list
    (swap-space
     (target (file-system-label "swap")))))
  (groups %java281-local-groups)
  (users
   (cons*
    (user-account
     (name "alexforsale")
     (comment "Kristian Alexander P")
     (uid 1000)
     (home-directory "/home/alexforsale")
     (group "alexforsale")
     (supplementary-groups '("wheel" "netdev" "users" "realtime"
                             "input" "lp" "audio" "video" "network-manager")))
    %java281-local-users))
  (packages
   (append
    %java281-desktop-packages
    %i3-packages
    %exwm-packages
    %stumpwm-packages
    %bash-packages
    %java281-basic-packages))
  (services
   (append
    (list
     ;; Add support for the SPICE protocol, which enables dynamic
     ;; resizing of the guest screen resolution, clipboard
     ;; integration with the host, etc.
     (service spice-vdagent-service-type)

     ;; (simple-service 'cron-jobs mcron-service-type
     ;;                 (list auto-update-resolution-crutch))
     (service sddm-service-type
              (sddm-configuration
               ;;(theme "maya")
               (hide-users "admin-local")
               (xorg-configuration
                (xorg-configuration
                 (modules (cons xf86-video-intel
                                %default-xorg-modules))
                 (keyboard-layout keyboard-layout)
                 (extra-config (list %xorg-libinput-config)))))))
    %java281-common-nongnu-services
    %java281-common-services
    %java281-senegal-services))
  (name-service-switch %mdns-host-lookup-nss))
