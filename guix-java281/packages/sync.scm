(define-module (guix-java281 packages sync)
  #:use-module (guix build-system cmake)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (gnu packages base)
  #:use-module (gnu packages boost)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages curl)
  #:use-module (gnu packages check)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages gnupg)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages web)
  #:use-module (gnu packages xml)
  )

;; FIXME:
;; build error
;; /gnu/store/zyqimpkmpffc24nwwqp46cicj8ss85y5-binutils-2.37/bin/ld: /gnu/store/0i6gfz0aqwnlppkr1c94z2ngs87q2zwp-curl-7.79.1/lib/libcurl.so: undefined reference to symbol 'inflateEnd'
;; /gnu/store/zyqimpkmpffc24nwwqp46cicj8ss85y5-binutils-2.37/bin/ld: /gnu/store/8qv5kb2fgm4c3bf70zcg9l6hkf3qzpw9-zlib-1.2.11/lib/libz.so.1: error adding symbols: DSO missing from command line
(define-public grive2
  (let ((commit "6901fbb169eaae51acb61778760ba2ac019675ae")
        (revision "0"))
    (package
      (name "grive2")
      (version "0.5.2")
      (source (origin
                (method git-fetch)
                (uri
                 (git-reference
                  (url "https://github.com/vitalif/grive2")
                  (commit commit)))
                (file-name (git-file-name name version))
                (sha256
                 (base32
                  "0x2bj2qa9k5qkg55w7lni137qa9r5srxrhbldsclr5fq4cljk3mz"))))
      (build-system cmake-build-system)
      (propagated-inputs
       `(("inotify-tools" ,inotify-tools)))
      (native-inputs
       `(("pkg-config" ,pkg-config)))
      (inputs
       `(;;("cppunit" ,cppunit)
         ;;("gcc:lib" ,gcc "lib")
         ;;("json-c" ,json-c)
         ("libgcrypt" ,libgcrypt)
         ;;("binutils" ,binutils)
         ("boost" ,boost)
         ("curl" ,curl)
         ("libyajl" ,libyajl)
         ("zlib" ,zlib)
         ("expat" ,expat)
         ("libiberty" ,libiberty)))
      (arguments
       '(#:build-type "Release"
         #:configure-flags `("-DCMAKE_EXE_LINKER_FLAGS=-lz")
         #:tests? #f))
      (home-page "https://github.com/vitalif/grive2")
      (synopsis "Google Drive sync")
      (description "Google Drive client with support for new
Drive REST API and partial sync")
      (license license:gpl2))))
