(define-module (guix-java281 packages emacs)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (guix build-system emacs)
  #:use-module (gnu packages emacs-xyz)
  #:use-module (gnu packages mail)
  #:use-module ((guix licenses) #:prefix license:))

(define-public emacs-vertico-posframe
  (let ((commit "7ca364d319e7ba8ccba26a0d57513f3e66f1b05b")
        (revision "0"))
    (package
      (name "emacs-vertico-posframe")
      (version (git-version "0.5.4" revision commit))
      (source
       (origin
         (uri (git-reference
               (url "https://github.com/tumashu/vertico-posframe")
               (commit commit)))
         (method git-fetch)
         (sha256
          (base32 "06fx69x0hiv4z56prplaciwnf3w14a4qx7srbkd1mqyq84sv37jy"))
         (file-name (git-file-name name version))))
      (build-system emacs-build-system)
      (propagated-inputs
       `(("emacs-posframe" ,emacs-posframe)
         ("emacs-vertico" ,emacs-vertico)))
      (home-page "https://github.com/tumashu/vertico-posframe")
      (synopsis "Emacs vertico extension for posframe")
      (description "Vertico extension which uses posframe to show its candidate menu.")
      (license license:gpl3+))))

(define-public emacs-logito
  (let ((commit "d5934ce10ba3a70d3fcfb94d742ce3b9136ce124")
        (revision "0"))
    (package
      (name "emacs-logito")
      (version (git-version "0.2.0" revision commit))
      (source
       (origin
         (uri (git-reference
               (url "https://github.com/sigma/logito")
               (commit commit)))
         (method git-fetch)
         (sha256
          (base32 "0bnkc6smvaq37q08q1wbrxw9mlcfbrax304fxw4fx7pc1587av0d"))
         (file-name (git-file-name name version))))
      (build-system emacs-build-system)
      (home-page "https://github.com/sigma/logito")
      (synopsis "Tiny logging framework for Emacs")
      (description "Tiny logging framework for Emacs")
      (license license:gpl2+))))

(define-public emacs-pcache
  (let ((commit "893d2a637423bae2cc5e72c559e3a9d1518797c9")
        (revision "0"))
    (package
      (name "emacs-pcache")
      (version (git-version "0.5.1" revision commit))
      (source
       (origin
         (uri (git-reference
               (url "https://github.com/sigma/pcache")
               (commit commit)))
         (method git-fetch)
         (sha256
          (base32 "1xxbi4lp4ygvciqmy8lp6zn47k954ziz5d95qz1l7a2jix3rxf1p"))
         (file-name (git-file-name name version))))
      (build-system emacs-build-system)
      (home-page "https://github.com/sigma/pcache")
      (synopsis "Persistent caching for Emacs")
      (description "Persistent caching for Emacs")
      (license license:gpl2+))))

(define-public emacs-marshal
  (let ((commit "490496d974d03906f784707ecc2e0ac36ed84b96")
        (revision "0"))
    (package
      (name "emacs-marshal")
      (version (git-version "0.9.1" revision commit))
      (source
       (origin
         (uri (git-reference
               (url "https://github.com/sigma/marshal.el")
               (commit commit)))
         (method git-fetch)
         (sha256
          (base32 "13yf61sw5rmqb8dshk1v9j348jkdfqql55dqvs9srb3ypj8b02v9"))
         (file-name (git-file-name name version))))
      (build-system emacs-build-system)
      (home-page "https://github.com/sigma/marshal.el")
      (synopsis "EIEIO marshalling, inspired by Go tagged structs")
      (description "EIEIO marshalling, inspired by Go tagged structs.")
      (license license:gpl2+))))

(define-public emacs-mocker
  (let ((commit "5b01b3cc51388faf1ba823683c3600790099c84c")
        (revision "0"))
    (package
      (name "emacs-mocker")
      (version (git-version "0.5.0" revision commit))
      (source
       (origin
         (uri (git-reference
               (url "https://github.com/sigma/mocker.el")
               (commit commit)))
         (method git-fetch)
         (sha256
          (base32 "0nmi6bsbbgcxihjb865bmm2zrirnzi1lq02d6cl1df57k47md4ny"))
         (file-name (git-file-name name version))))
      (build-system emacs-build-system)
      (home-page "https://github.com/sigma/mocker.el")
      (synopsis "Simple mocking framework for Emacs")
      (description "A simple mocking framework for Emacs")
      (license license:gpl2+))))

(define-public emacs-undercover
  (let ((commit "1d3587f1fad66a747688f36636b67b33b73447d3")
        (revision "0"))
    (package
      (name "emacs-undercover")
      (version (git-version "0.8.1" revision commit))
      (source
       (origin
         (uri (git-reference
               (url "https://github.com/undercover-el/undercover.el")
               (commit commit)))
         (method git-fetch)
         (sha256
          (base32 "0qmvyy3xg5qi7ws8zcs934d6afsappr1a6pgfp796xpa9vdr4y6j"))
         (file-name (git-file-name name version))))
      (build-system emacs-build-system)
      (propagated-inputs
       `(("emacs-dash" ,emacs-dash)
         ("emacs-shut-up" ,emacs-shut-up)))
      (home-page "https://github.com/undercover-el/undercover.el")
      (synopsis "Test coverage library for Emacs")
      (description "A test coverage library for Emacs")
      (license license:gpl2+))))

;; (define-public emacs-gh
;;   (let ((commit "27ccc892e94f7e747e5b879eec71119965d9ed6e")
;;         (revision "0"))
;;     (package
;;       (name "emacs-gh")
;;       (version (git-version "1.0.1" revision commit))
;;       (source
;;        (origin
;;          (uri (git-reference
;;                (url "https://github.com/sigma/gh.el.git")
;;                (commit commit)))
;;          (method git-fetch)
;;          (sha256
;;           (base32 "0biljpqw14kjv0njyrahfdgglaphghva0kpfjhiprfwbd0rmmi1k"))
;;          (file-name (git-file-name name version))))
;;       (build-system emacs-build-system)
;;       (propagated-inputs
;;        `(("emacs-logito" ,emacs-logito)
;;          ("emacs-pcache" ,emacs-pcache)
;;          ("emacs-marshal" ,emacs-marshal)
;;          ("emacs-mocker" ,emacs-mocker)
;;          ("emacs-undercover" ,emacs-undercover)))
;;       (home-page "https://github.com/sigma/gh.el.git")
;;       (synopsis "Github API library for Emacs")
;;       (description "This library also allows implementation of the various authentication schemes (password, OAuth).")
;;       (license license:gpl2+))))

(define-public emacs-consult-lsp
  (let ((commit "19606a03cf854e1b0930c4526ed92c4560dccdc2")
        (revision "0"))
    (package
      (name "emacs-consult-lsp")
      (version (git-version "1.0.0" revision commit))
      (source
       (origin
         (uri (git-reference
               (url "https://github.com/gagbo/consult-lsp")
               (commit commit)))
         (method git-fetch)
         (sha256
          (base32 "1g4wimqghcph6jrk9y7sqk2pqppx63n4z0557fpgwd57x62rl7zv"))
         (file-name (git-file-name name version))))
      (build-system emacs-build-system)
      (propagated-inputs
       `(("emacs-consult" ,emacs-consult)
         ("emacs-lsp-mode" ,emacs-lsp-mode)
         ("emacs-f" ,emacs-f)))
      (home-page "https://github.com/gagbo/consult-lsp")
      (synopsis "LSP-mode and consult.el helping each other")
      (description "LSP-mode and consult.el helping each other")
      (license license:gpl2+))))

(define-public emacs-consult-projectile
  (let ((commit "5ef1ada3be767ea766255801050210f5d796deec")
        (revision "0"))
    (package
      (name "emacs-consult-projectile")
      (version (git-version "0.0.1" revision commit))
      (source
       (origin
         (uri (git-reference
               (url "https://gitlab.com/OlMon/consult-projectile")
               (commit commit)))
         (method git-fetch)
         (sha256
          (base32 "1x0h9jbgkm32gs75hha3cwk5l24mpq0lmsik03msnc4wl2kvpnhm"))
         (file-name (git-file-name name version))))
      (build-system emacs-build-system)
      (propagated-inputs
       `(("emacs-consult" ,emacs-consult)
         ("emacs-projectile" ,emacs-projectile)))
      (home-page "https://gitlab.com/OlMon/consult-projectile")
      (synopsis "Projectile integration for consult")
      (description "A package to incorporate projectile into consult.
  This allow to choose a project, when none is selected or
  choose a project buffer/file.")
      (license license:gpl3+))))

(define-public emacs-consult-org-roam
  (let ((commit "9b51aed939054c54934a6969290ad78587051cde")
        (revision "0"))
    (package
      (name "emacs-consult-org-roam")
      (version (git-version "0.0.1" revision commit))
      (source
       (origin
         (uri (git-reference
               (url "https://github.com/jgru/consult-org-roam")
               (commit commit)))
         (method git-fetch)
         (sha256
          (base32 "160rix04azd1wc7fls29bhssjwzwmmnnqcfbkvapsyryvn80q219"))
         (file-name (git-file-name name version))))
      (build-system emacs-build-system)
      (propagated-inputs
       `(("emacs-consult" ,emacs-consult)
         ("emacs-org-roam" ,emacs-org-roam)))
      (home-page "https://github.com/jgru/consult-org-roam")
      (description "Collection of functions to operate org-roam with the help of consult
and its live preview feature")
      (synopsis "Convenience functions for operating org-roam with the help of consult")
      (license license:gpl3+))))

(define-public emacs-consult-yasnippet
  (let ((commit "cdb256d2c50e4f8473c6052e1009441b65b8f8ab")
        (revision "0"))
    (package
      (name "emacs-consult-yasnippet")
      (version (git-version "0.0.1" revision commit))
      (source
       (origin
         (uri (git-reference
               (url "https://github.com/mohkale/consult-yasnippet")
               (commit commit)))
         (method git-fetch)
         (sha256
          (base32 "0sr0v6kd91sbz8zfg35b5y2s3mr047a75kwh9himn2jgrm75kl50"))
         (file-name (git-file-name name version))))
      (build-system emacs-build-system)
      (propagated-inputs
       `(("emacs-consult" ,emacs-consult)
         ("emacs-yasnippet" ,emacs-yasnippet)))
      (home-page "https://github.com/mohkale/consult-yasnippet")
      (synopsis "Consulting-read interface for yasnippet")
      (description "A consulting-read interface for yasnippet.")
      (license license:gpl3+))))

(define-public emacs-consult-flycheck
  (let ((commit "9b40f136c017fadf6239d7602d16bf73b4ad5198")
        (revision "0"))
    (package
      (name "emacs-consult-flycheck")
      (version (git-version "0.8.0" revision commit))
      (source
       (origin
         (uri (git-reference
               (url "https://github.com/minad/consult-flycheck")
               (commit commit)))
         (method git-fetch)
         (sha256
          (base32 "0m30kz2ixxih433kddkyi0sfh7hkwm746417l2f30b273vlfj7ya"))
         (file-name (git-file-name name version))))
      (build-system emacs-build-system)
      (propagated-inputs
       `(("emacs-consult" ,emacs-consult)
         ("emacs-flycheck" ,emacs-flycheck)))
      (home-page "https://github.com/minad/consult-flycheck")
      (synopsis "Integrates Consult with Flycheck")
      (description "Integrates Consult with Flycheck.")
      (license license:gpl3+))))

(define-public emacs-block-nav
  (let ((commit "d69acaa3d6c75bf4c518d8ab8896ad63580253fc")
        (revision "0"))
    (package
      (name "emacs-block-nav")
      (version (git-version "0.0.1" revision commit))
      (source
       (origin
         (uri (git-reference
               (url "https://github.com/nixin72/block-nav.el")
               (commit commit)))
         (method git-fetch)
         (sha256
          (base32 "14qnak1ynfzjlx8dp82dwmz2qhwrjyiz6zpaa7kpca4zgn8xgc5p"))
         (file-name (git-file-name name version))))
      (build-system emacs-build-system)
      (home-page "https://github.com/nixin72/block-nav.el")
      (synopsis "Emacs package to navigate based on block levels")
      (description "An Emacs package to navigate based on block levels.")
      (license license:gpl3+))))

(define-public emacs-ansible
  (let ((commit "d89ac0ee57742cca0f0e0a3453d9dcc521575690")
        (revision "0"))
    (package
      (name "emacs-ansible")
      (version (git-version "0.3.2" revision commit))
      (source
       (origin
         (uri (git-reference
               (url "https://github.com/k1LoW/emacs-ansible")
               (commit commit)))
         (method git-fetch)
         (sha256
          (base32 "1n38cvdpp2d00vl7ky4qf820rylffkapa3d9s4pwjw6lj55f00ak"))
         (file-name (git-file-name name version))))
      (build-system emacs-build-system)
      (propagated-inputs
       `(("emacs-s" ,emacs-s)
         ("emacs-f" ,emacs-f)))
      (home-page "https://github.com/k1LoW/emacs-ansible")
      (synopsis "Ansible minor mode for Emacs")
      (description "Ansible minor mode for Emacs")
      (license license:gpl3+))))

(define-public emacs-evil-easymotion
  (let ((commit "f96c2ed38ddc07908db7c3c11bcd6285a3e8c2e9")
        (revision "0"))
    (package
      (name "emacs-evil-easymotion")
      (version (git-version "0.0.1" revision commit))
      (source
       (origin
         (uri (git-reference
               (url "https://github.com/PythonNut/evil-easymotion")
               (commit commit)))
         (method git-fetch)
         (sha256
          (base32 "0xsva9bnlfwfmccm38qh3yvn4jr9za5rxqn4pwxbmhnx4rk47cch"))
         (file-name (git-file-name name version))))
      (build-system emacs-build-system)
      (propagated-inputs
       `(("emacs-evil" ,emacs-evil)
         ("emacs-avy" ,emacs-avy)))
      (home-page "https://github.com/PythonNut/evil-easymotion")
      (synopsis "Port of vim easymotion to Emacs' evil-mode")
      (description "A port of vim easymotion to Emacs' evil-mode")
      (license license:gpl3+))))

(define-public emacs-embrace
  (let ((commit "dd5da196e5bcc5e6d87e1937eca0c21da4334ef2")
        (revision "0"))
    (package
      (name "emacs-embrace")
      (version (git-version "0.1.4" revision commit))
      (source
       (origin
         (uri (git-reference
               (url "https://github.com/cute-jumper/embrace.el")
               (commit commit)))
         (method git-fetch)
         (sha256
          (base32 "1m0qyipkp5ydgcav8d0m58fbj1gilipbj7g8mg40iajr8wfqcjdc"))
         (file-name (git-file-name name version))))
      (build-system emacs-build-system)
      (propagated-inputs
       `(("emacs-expand-region" ,emacs-expand-region)))
      (home-page "https://github.com/cute-jumper/embrace.el")
      (synopsis "Add/Change/Delete pairs based on expand-region,
similar to evil-surround")
      (description "Add/Change/Delete pairs based on expand-region,
similar to evil-surround.")
      (license license:gpl3+))))

(define-public emacs-evil-embrace
  (let ((commit "7b5a539cfe7db238d860122c793a0cb2d329cc6e")
        (revision "0"))
    (package
      (name "emacs-evil-embrace")
      (version (git-version "0.1.1" revision commit))
      (source
       (origin
         (uri (git-reference
               (url "https://github.com/cute-jumper/evil-embrace.el")
               (commit commit)))
         (method git-fetch)
         (sha256
          (base32 "03b53626ywq9qdqzsb92321lc0fzjqb674kwkssjrxlz6hhn5hlq"))
         (file-name (git-file-name name version))))
      (build-system emacs-build-system)
      (propagated-inputs
       `(("emacs-embrace" ,emacs-embrace)
         ("emacs-evil-surround" ,emacs-evil-surround)))
      (home-page "https://github.com/cute-jumper/evil-embrace.el")
      (synopsis "Evil integration of embrace.el")
      (description "Evil integration of embrace.el")
      (license license:gpl3+))))

(define-public emacs-selectric-mode
  (let ((commit "1840de71f7414b7cd6ce425747c8e26a413233aa")
        (revision "0"))
    (package
      (name "emacs-selectric-mode")
      (version (git-version "1.4.2" revision commit))
      (source
       (origin
         (uri (git-reference
               (url "https://github.com/rbanffy/selectric-mode")
               (commit commit)))
         (method git-fetch)
         (sha256
          (base32 "1aabqqqcafkqmyarf5kb1k0gglmlpn6kr3h3x0yph5gd6sk3l4ll"))
         (file-name (git-file-name name version))))
      (build-system emacs-build-system)
      (home-page "https://github.com/rbanffy/selectric-mode")
      (synopsis "Make your Emacs sound like a proper typewriter")
      (description "Make your Emacs sound like a proper typewriter.")
      (license license:gpl3+))))

(define-public emacs-ol-notmuch
  (let ((commit "1a53d6c707514784cabf33d865b577bf77f45913")
        (revision "0"))
    (package
      (name "emacs-ol-notmuch")
      (version (git-version "2.0.0" revision commit))
      (source
       (origin
         (uri (git-reference
               (url "https://git.sr.ht/~tarsius/ol-notmuch")
               (commit commit)))
         (method git-fetch)
         (sha256
          (base32 "16p7j51z8r047alwn2hkb6944f7ds29ckb97b4k8ia00vwch0d67"))
         (file-name (git-file-name name version))))
      (build-system emacs-build-system)
      (propagated-inputs
       `(("emacs-compat" ,emacs-compat)
         ("emacs-notmuch" ,emacs-notmuch)))
      (home-page "https://git.sr.ht/~tarsius/ol-notmuch")
      (synopsis "Links to Notmuch buffers from Org documents")
      (description "This package implements links to notmuch messages and searches.
A search is a query to be performed by notmuch; it is the equivalent to folders in
other mail clients. Similarly, mails are referred to by a query,
so both a link can refer to several mails.")
      (license license:gpl3+))))

(define-public emacs-perspective-exwm
  (let ((commit "8afdbf894a888854ce9dfbe0ad2a5dc41f75ecb8")
        (revision "0"))
    (package
      (name "emacs-perspective-exwm")
      (version (git-version "0.1.4" revision commit))
      (source
       (origin
         (uri (git-reference
               (url "https://github.com/SqrtMinusOne/perspective-exwm.el")
               (commit commit)))
         (method git-fetch)
         (sha256
          (base32 "191xm4l5id480bcf2nlliacrn2a9qrxs18pfkd4sk4bn9xxz74dx"))
         (file-name (git-file-name name version))))
      (build-system emacs-build-system)
      (propagated-inputs
       `(("emacs-burly" ,emacs-burly)
         ("emacs-exwm" ,emacs-exwm)
         ("emacs-perspective" ,emacs-perspective)))
      (home-page "https://github.com/SqrtMinusOne/perspective-exwm.el")
      (synopsis "perspective.el heart EXWM")
      (description "A couple of tricks and fixes to make using EXWM
and perspective.el a better experience.")
      (license license:gpl3+))))
