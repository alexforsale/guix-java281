(define-module (guix-java281 packages)
  #:use-module (srfi srfi-1)
  #:use-module (guix build-system cmake)
  #:use-module (guix download)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (guix channels)
  #:use-module (gnu packages)
  #:use-module ((guix licenses) #:prefix license:))

(define (search-patch file-name)
  "Search the patch FILE-NAME.  Raise an error if not found."
  (or (search-path (%rde-patch-path) file-name)
      (raise (formatted-message (G_ "~a: patch not found")
                                file-name))))


(define-syntax-rule (search-patches file-name ...)
  "Return the list of absolute file names corresponding to each
FILE-NAME found in %PATCH-PATH."
  (list (search-patch file-name) ...))

(define %channel-root
  (find (lambda (path)
          (file-exists? (string-append path "/guix-java281/packages.scm")))
        %load-path))

(define %java281-patch-path
  (make-parameter
   (append
    (list
     (string-append %channel-root "packages/patches"))
    (%patch-path))))
