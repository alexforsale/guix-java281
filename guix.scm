(define-module (guix))
(use-modules (guix gexp)
             (guix packages)
             (guix git-download)
             (guix build-system trivial)
             (guix-java281 packages emacs)
             (guix-java281 packages sync)
             (guix-java281 home modules emacs)
             (guix-java281 home modules mail)
             (gnu packages base)
             (gnu packages texinfo)
             ((guix licenses) #:prefix license:))

;; list of my defined apps
grive2
emacs-vertico-posframe
emacs-logito
emacs-pcache
emacs-marshal
emacs-mocker
emacs-undercover
emacs-consult-lsp
emacs-consult-projectile
emacs-consult-org-roam
emacs-consult-yasnippet
emacs-consult-flycheck
emacs-block-nav
emacs-ansible
emacs-evil-easymotion
emacs-embrace
emacs-evil-embrace
emacs-selectric-mode
mail-scripts
